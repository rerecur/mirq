package xyz.mirq.structures;

import xyz.mirq.typeclasses.Order;

/**
 * An <code>OrderedRing</code> is <code>Ring</code> with meaningful <code>signum</code> and
 * <code>abs</code> methods. We also have convenience methods like <code>isPositive</code>.
 *
 * @param <S> The <code>Self</code> type that inherits from <code>OrderedRing</code>.
 */
public interface OrderedRing<S extends OrderedRing<S>>
	extends Ring<S>, Order<S>, Signed<S> {

	@Override
	default boolean isPositive() {
		return getZero().lt(this.self());
	}

	@Override
	default boolean isNegative() {
		return this.lt(getZero());
	}

	@Override
	default int signum() {
		/* Defend against unexpected values from compareTo in case it's overwritten */
		return Integer.signum(this.compareTo(getZero()));
	}

	@Override
	default S abs() {
		return isNegative() ? opposite() : self();
	}

}
