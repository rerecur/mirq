package xyz.mirq.structures;

/**
 * <code>Ring</code> elements form a commutative additive group and a multiplicative monoid, where
 * multiplication distributes over addition. These properties are not enforced within the type
 * system, however. For example, the integers ZZ form a ring.
 * <p>
 * Example Usage:
 * <pre>
 *  class Foo implements Ring&lt;Foo&gt; {
 *     int n;
 *
 *     &#064;Override
 *     public Foo opposite() {
 *         final Foo foo = new Foo();
 *         foo.n = -this.n;
 *         return foo;
 *     }
 *
 *     &#064;Override
 *     public Foo getZero() {
 *         final Foo zero = new Foo();
 *         zero.n = 0;
 *         return zero;
 *     }
 *
 *     &#064;Override
 *     public Foo plus(final Foo that) {
 *         final Foo sum = new Foo();
 *         sum.n = this.n + that.n;
 *         return sum;
 *     }
 *
 *     &#064;Override
 *     public Foo getUnit() {
 *         final Foo unit = new Foo();
 *         unit.n = 1;
 *         return unit;
 *     }
 *
 *     &#064;Override
 *     public Foo times(final Foo multiplicand) {
 *         final Foo product = new Foo();
 *         product.n = this.n * multiplicand.n;
 *         return product;
 *     }
 *
 *     &#064;Override
 *     public boolean eqv(final Foo that) {
 *         return this.n == that.n;
 *     }
 *
 *     &#064;Override
 *     public Foo self() {
 *         return this;
 *     }
 *  }
 * </pre>
 * </p>
 * @param <S> The <code>Self</code> type that inherits from <code>Ring&lt;S&gt;</code>.
 */
public interface Ring<S extends Ring<S>>
	extends AdditiveGroup<S>, MultiplicativeMonoid<S> {

	/**
	 * Computes the commutator, <code>[this, that] = this * that - that * this</code>, which should
	 * always be zero if multiplication is commutative.
	 */
	default S commutator(S that) {
		return this.times(that).minus(that.times(this.self()));
	}
}
