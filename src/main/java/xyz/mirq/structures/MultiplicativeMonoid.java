package xyz.mirq.structures;

/**
 * <code>MultiplicativeMonoid</code> elements form an additive semigroup with a neutral (or
 * identity) element, unit (or one). The neutral property (and the uniqueness of the neutral
 * element) is not enforced within the type system, however. For example, the non-negative integers
 * form an multiplicative monoid.
 * <p>
 * <pre>
 *  class Foo implements MultiplicativeMonoid&lt;Foo&gt; {
 *     int n;
 *
 *     &#064;Override
 *     public Foo getUnit() {
 *         final Foo unit = new Foo();
 *         unit.n = 1;
 *         return unit;
 *     }
 *
 *     &#064;Override
 *     public Foo times(final Foo multiplicand) {
 *         final Foo product = new Foo();
 *         product.n = this.n * multiplicand.n;
 *         return product;
 *     }
 *
 *     &#064;Override
 *     public boolean eqv(final Foo that) {
 *         return this.n == that.n;
 *     }
 *
 *     &#064;Override
 *     public Foo self() {
 *         return this;
 *     }
 *  }
 * </pre>
 * </p>
 * @param <S> The <code>Self</code> type that inherits from
 *  <code>MultiplicativeMonoid&lt;S&gt;</code>.
 */
public interface MultiplicativeMonoid<S extends MultiplicativeMonoid<S>>
	extends MultiplicativeSemiGroup<S>, HasUnit<S> {

}
