package xyz.mirq.structures;

/**
 * An <code>AdditiveSmallCategory</code> is an additive monoid, but without requiring that the
 * combining operation be closed. In other words, it's an additive semi groupoid, with the zero
 * neutral element included.
 */
public interface AdditiveSmallCategory<S extends AdditiveSmallCategory<S>>
	extends AdditiveSemiGroupoid<S>, HasZero<S> {

}
