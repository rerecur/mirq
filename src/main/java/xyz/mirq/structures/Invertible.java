package xyz.mirq.structures;

import xyz.mirq.typeclasses.Self;

import java.util.Optional;

/**
 * <code>Invertible</code> elements comprise a group or groupoid structure, in which inverses make
 * sense. However this interface makes no assumptions about any combine-like operations, which would
 * be <code>times</code> in the case of fields and multiplicative groups, <code>apply</code> in the
 * case of functions, and so on.
 * <p>
 *     Since inverses need not always exist (as in the case of matrices), the <code>inverse</code>
 *     method returns an <code>Optional</code>, rather than throwing an exception. We also offer
 *     a convenience <code>reciprocal</code> method, which instead should throw an
 *     <code>ArithmeticException</code> when the inverse does not exist.
 * </p>
 *
 * @param <S>
 */
public interface Invertible<S extends Self<S>> {

	/**
	 * Compute the <code>Optional</code> inverse of <code>this</code>, or return
	 * <code>Optional.empty()</code> if the inverse does not exist. Typically for a multiplicative
	 * group or groupoid structure, the inverse is the multiplicative inverse, or reciprocal.
	 * <p>
	 *     By convention, we prefer overlines to indicate inverses (rather than, say, complex
	 *     conjugation).
	 * </p>
	 *
	 * @return <code>Optional(this⁻¹) == Optional(T̅H̅I̅S̅)</code>
	 */
	Optional<S> inverse();

	/**
	 * Convenience method to compute the multiplicative inverse of <code>this</code>. For a true
	 * group, the inverse always exists, and so this method should not throw
	 * <code>ArithmeticException</code> in such cases. But if this type is used to describe
	 * near-groups, such as matrices or fields, then this method could throw to indicate the
	 * reciprocal does not exist.
	 *
	 * @return <code>1 / this</code>
	 *
	 * @throws ArithmeticException If this is not a true group, and the reciprocal does not exist.
	 */
	default S reciprocal() throws ArithmeticException {
		return inverse().orElseThrow(() -> new ArithmeticException("/ by zero"));
	}

}
