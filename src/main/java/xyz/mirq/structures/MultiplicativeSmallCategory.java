package xyz.mirq.structures;

/**
 * A <code>MultiplicativeSmallCategory</code> is a multiplicative monoid, but without requiring that
 * the combining operation be closed. In other words, it's a multiplicative semi groupoid, with the
 * unit neutral element included.
 */
public interface MultiplicativeSmallCategory<S extends MultiplicativeSmallCategory<S>>
	extends MultiplicativeSemiGroupoid<S>, HasUnit<S> {
}
