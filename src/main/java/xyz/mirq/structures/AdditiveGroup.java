package xyz.mirq.structures;

/**
 * <code>AdditiveGroup</code> elements form an additive monoid where every element has an additive
 * inverse, or opposite. This property is not enforced within the type system, however. For
 * example, the integers ZZ form an additive group.
 * <p>
 * <pre>
 *  class Foo implements AdditiveGroup&lt;Foo&gt; {
 *     int n;
 *
 *     &#064;Override
 *     public Foo opposite() {
 *         final Foo foo = new Foo();
 *         foo.n = -this.n;
 *         return foo;
 *     }
 *
 *     &#064;Override
 *     public Foo getZero() {
 *         final Foo zero = new Foo();
 *         zero.n = 0;
 *         return zero;
 *     }
 *
 *     &#064;Override
 *     public Foo plus(final Foo that) {
 *         final Foo sum = new Foo();
 *         sum.n = this.n + that.n;
 *         return sum;
 *     }
 *
 *     &#064;Override
 *     public boolean eqv(final Foo that) {
 *         return this.n == that.n;
 *     }
 *
 *     &#064;Override
 *     public Foo self() {
 *         return this;
 *     }
 *  }
 * </pre>
 * </p>
 * @param <S> The <code>Self</code> type that inherits from <code>AdditiveGroup&lt;S&gt;</code>.
 */
public interface AdditiveGroup<S extends AdditiveGroup<S>>
	extends AdditiveMonoid<S> {

	/**
	 * Compute the additive inverse of <code>this</code>.
	 *
	 * @return <code>-this</code>
	 */
	S opposite();

	/**
	 * Compute the difference <code>this - that</code>.
	 *
	 * @param that The subtrahend to be subtracted from <code>this</code> minuend.
	 *
	 * @return <code>this - that</code>
	 */
	default S minus(final S that) {
		return this.plus(that.opposite());
	}
}
