package xyz.mirq.structures;

import xyz.mirq.typeclasses.Eq;

import java.util.Optional;

/**
 * A <code>MultiplicativeSemiGroupoid</code> is nearly a multiplicative semigroup, but without
 * requiring that the combining operation be closed. In other words, the <code>times</code>
 * operation returns an <code>Optional</code> instead of the <code>Self</code> type.
 */
public interface MultiplicativeSemiGroupoid<S extends MultiplicativeSemiGroupoid<S>>
	extends Eq<S> {

	/**
	 * Compute the product <code>this * multiplicand</code>, which because of the lack of closure,
	 * might not exist.
	 *
	 * @param multiplicand The right hand side to be multiplied by <code>this</code> multiplier.
	 *
	 * @return <code>Optional(this * multiplicand)</code>
	 */
	Optional<S> times(final S multiplicand);

	/**
	 * Compute the product <code>multiplier * this</code>, which is equivalend to the
	 * <code>times</code> method if this groupoid is commutative. Non-commutative groupoids should
	 * override the default <code>productByMultiplier</code> implementation.
	 *
	 * @param multiplier The left hand side of this multiplication.
	 *
	 * @return <code>Optional(multiplier * this)</code>
	 */
	default Optional<S> productByMultiplier(final S multiplier) {
		return multiplier.times(this.self());
	}

}
