package xyz.mirq.structures;

import xyz.mirq.typeclasses.Eq;

import java.util.Optional;

/**
 * An <code>AdditiveSemiGroupoid</code> is nearly an additive semigroup, but without requiring that
 * the combining operation be closed. In other words, the <code>plus</code> operation returns an
 * <code>Optional</code> instead of the <code>Self</code> type.
 */
public interface AdditiveSemiGroupoid<S extends AdditiveSemiGroupoid<S>>
	extends Eq<S> {

	/**
	 * Compute the sum <code>this + addend</code>, which because of the lack of closure, might not
	 * exist.
	 *
	 * @param addend The right hand side to be added to <code>this</code> augend.
	 *
	 * @return <code>Optional(this + addend)</code>
	 */
	Optional<S> plus(final S addend);

	/**
	 * Compute the sum <code>augend + this</code>, which is equivalent to the <code>plus</code>
	 * method if this groupoid is commutative. Non-commutative groupoids should override the default
	 * <code>sumByAugend</code> implementation.
	 *
	 * @param augend The left hand side of the addition.
	 *
	 * @return <code>Optional(augend + this)</code>
	 */
	default Optional<S> sumByAugend(final S augend) {
		return augend.plus(this.self());
	}

}
