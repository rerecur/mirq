package xyz.mirq.structures;

/**
 * An <code>OrderedField</code> is a <code>Field</code> built from an <code>OrderedRing</code>. It
 * enjoys methods like <code>lt</code>, <code>signum</code>, <code>abs</code>, and so forth.
 *
 * @param <S> The <code>Self</code> type that inherits from <code>OrderedField</code>.
 */
public interface OrderedField<S extends OrderedField<S>>
	extends Field<S>, OrderedRing<S> {

}
