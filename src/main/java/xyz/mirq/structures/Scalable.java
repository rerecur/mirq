package xyz.mirq.structures;

import xyz.mirq.typeclasses.Self;

public interface Scalable<F extends Field<F>, S extends Scalable<F, S>> /*extends Self<S>*/ {

	S scaleBy(F scalar);
}
