package xyz.mirq.structures;

/**
 * <code>AdditiveMonoid</code> elements form an additive semigroup with a neutral (or identity)
 * element, zero. The neutral property (and the uniqueness of the neutral element) is not enforced
 * within the type system, however. For example, the non-negative integers form an additive monoid.
 * <p>
 * <pre>
 *  class Foo implements AdditiveMonoid&lt;Foo&gt; {
 *     int n;
 *
 *     &#064;Override
 *     public Foo getZero() {
 *         final Foo zero = new Foo();
 *         zero.n = 0;
 *         return zero;
 *     }
 *
 *     &#064;Override
 *     public Foo plus(final Foo that) {
 *         final Foo sum = new Foo();
 *         sum.n = this.n + that.n;
 *         return sum;
 *     }
 *
 *     &#064;Override
 *     public boolean eqv(final Foo that) {
 *         return this.n == that.n;
 *     }
 *
 *     &#064;Override
 *     public Foo self() {
 *         return this;
 *     }
 *  }
 * </pre>
 * </p>
 * @param <S> The <code>Self</code> type that inherits from <code>AdditiveMonoid&lt;S&gt;</code>.
 */
public interface AdditiveMonoid<S extends AdditiveMonoid<S>>
	extends AdditiveSemiGroup<S>, HasZero<S> {

}
