package xyz.mirq.structures;

import xyz.mirq.typeclasses.Eq;

/**
 * <code>MultiplicativeSemiGroup</code> elements enjoy a binary operation, <code>times</code>,
 * which is presumed to be closed and associative. These properties are not enforced within the
 * type system, however. For example, the positive integers form a multiplicative semigroup.
 * <p>
 * Example Usage:
 * <pre>
 *  class Foo implements MultiplicativeSemiGroup&lt;Foo&gt; {
 *     int n;
 *
 *     &#064;Override
 *     public Foo times(final Foo multiplicand) {
 *         final Foo product = new Foo();
 *         product.n = this.n * multiplicand.n;
 *         return product;
 *     }
 *
 *     &#064;Override
 *     public boolean eqv(final Foo that) {
 *         return this.n == that.n;
 *     }
 *
 *     &#064;Override
 *     public Foo self() {
 *         return this;
 *     }
 *  }
 * </pre>
 *
 * @param <S> The <code>Self</code> type that inherits from
 * 	<code>MultiplicativeSemiGroup&lt;S&gt;</code>.
 */
public interface MultiplicativeSemiGroup<S extends MultiplicativeSemiGroup<S>>
	extends Eq<S> {

	/**
	 * Compute the product <code>this * multiplicand</code>.
	 *
	 * @param multiplicand The right hand side to be multiplied by <code>this</code> multiplier.
	 *
	 * @return <code>this * that</code>
	 */
	S times(final S multiplicand);

	/**
	 * Compute the product <code>multiplier * this</code>, which is equivalent to the
	 * <code>times</code> method if this group is commutative. Non-commutative groups should
	 * override the default <code>productByMultiplier</code> implementation.
	 *
	 * @param multiplier The left hand side of the multiplication.
	 *
	 * @return <code>multiplier * this</code>
	 */
	default S productByMultiplier(final S multiplier) {
		return multiplier.times(this.self());
	}
}
