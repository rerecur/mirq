package xyz.mirq.structures;

import java.util.Optional;

/**
 * An <code>AdditiveGroupoid</code> is nearly an additive group, but without requiring that the
 * combining operation be closed. Alternatively, it's an additive small category with opposites
 * (additive inverses) included. In other words, the <code>minus</code> operation returns an
 * <code>Optional</code> instead of the <code>Self</code> type.
 */
public interface AdditiveGroupoid<S extends AdditiveGroupoid<S>>
	extends AdditiveSmallCategory<S> {

	/**
	 * Compute the additive inverse of <code>this</code>.
	 *
	 * @return <code>-this</code>
	 */
	S opposite();

	/**
	 * Compute the difference <code>this - that</code>, which may not exist because of the lack of
	 * closedness.
	 *
	 * @param that The subtrahend to be subtracted from <code>this</code> minuend.
	 *
	 * @return <code>Optional(this - that)</code>
	 */
	default Optional<S> minus(final S that) {
		return this.plus(that.opposite());
	}

}
