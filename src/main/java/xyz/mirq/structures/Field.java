package xyz.mirq.structures;

/**
 * A <code>Field</code> is a <code>Ring</code> where every element except zero has a reciprocal. Put
 * another way, the set of all of a field's elements except zero forms a multiplicative group. For
 * example, the rational * numbers QQ form a field.
 * <p>
 * When defining the <code>Field</code> below, we extend <code>MultiplicativeGroup</code> to get
 * the <code>reciprocal</code> method's signature and <code>over</code> method's default
 * implementation, even though zero has no multiplicative inverse. The <code>reciprocal</code>
 * method's implementation should throw a runtime <code>ArithmeticException</code> when called on
 * the zero element, or on any element for which the multiplicative inverse does not exist.
 * Likewise, if the <code>over</code> method is overridden, its implementation should do the same
 * whenever the divisor has no reciprocal.
 * </p>
 * <p>
 * <code>Field</code> elements can act as scalars, and multiply <code>Vector</code>s,
 * <code>Covector</code>s, and <code>Matrix</code> elements. However, since matrices themselves can
 * be fields, we cannot overload the <code>times</code> method, or else erasure will create an
 * ambiguity. Therefore, we introduce the <code>scale</code> and <code>scaleBy</code> methods, in
 * which this field element is the multiplier, and the vector, covector, or matrix is the
 * <code>Scalable</code> multiplicand.
 * </p>
 *
 * @param <S> The <code>Self</code> type that inherits from <code>Field&lt;S&gt;</code>.
 */
public interface Field<S extends Field<S>>
	extends Ring<S>, MultiplicativeGroup<S> {

	/**
	 * Compute the <code>Scalable</code> object formed by the product
	 * <code>this * multiplicand</code>. The implementation is obliged to respect this ordering of
	 * operands in case the field is not commutative. Typically, the multiplicand is a vector,
	 * covector, or matrix.
	 *
	 * @param multiplicand The <code>Scalable</code> to be scaled
	 * @param <M> The <code>Self</code> type of the multiplicand
	 * @return <code>this * multiplicand</code>
	 */
	default <M extends Scalable<S, M>> M scale(M multiplicand) {
		return multiplicand.scaleBy(self());
	}

}
