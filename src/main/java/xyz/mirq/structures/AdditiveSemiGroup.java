package xyz.mirq.structures;

import xyz.mirq.typeclasses.Eq;

/**
 * <code>AdditiveSemiGroup</code> elements enjoy a binary operation, <code>plus</code>, which is
 * presumed to be closed, associative, and commutative. These properties are not enforced within
 * the type system, however. For example, the positive integers form an additive semigroup.
 * <p>
 * Example Usage:
 * <pre>
 *  class Foo implements AdditiveSemiGroup&lt;Foo&gt; {
 *     int n;
 *
 *     &#064;Override
 *     public Foo plus(final Foo addend) {
 *         final Foo sum = new Foo();
 *         sum.n = this.n + that.n;
 *         return sum;
 *     }
 *
 *     &#064;Override
 *     public boolean eqv(final Foo that) {
 *         return this.n == that.n;
 *     }
 *
 *     &#064;Override
 *     public Foo self() {
 *         return this;
 *     }
 *  }
 * </pre>
 *
 * @param <S> The <code>Self</code> type that inherits from
 * 	<code>AdditiveSemiGroup&lt;S&gt;</code>.
 */
public interface AdditiveSemiGroup<S extends AdditiveSemiGroup<S>>
	extends Eq<S> {

	/**
	 * Compute the sum <code>this + addend</code>.
	 *
	 * @param addend The right had side to be added to <code>this</code> augend.
	 *
	 * @return <code>this + addend</code>
	 */
	S plus(final S addend);

	/**
	 * Compute the sum <code>augend + this</code>, which is equivalent to the <code>plus</code>
	 * method if this group is commutative. Non-commutative groups should override the default
	 * <code>sumByAugend</code> implementation.
	 *
	 * @param augend The left hand side of the addition.
	 *
	 * @return <code>augend + this</code>
	 */
	default S sumByAugend(final S augend) {
		return augend.plus(this.self());
	}
}
