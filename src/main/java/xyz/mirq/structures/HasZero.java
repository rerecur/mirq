package xyz.mirq.structures;

import xyz.mirq.typeclasses.Eq;

/**
 * <code>HasZero</code> is the shared functionality across <code>AdditiveMonoid</code> and
 * <code>AdditiveSmallGroup</code>. The methods defined here don't care whether the additive combine
 * operation, <code>plus</code>, is closed or not.
 *
 * @param <S> The <code>Self</code> type that inherits from <code>HasZero&lt;S&gt;</code>.
 */
public interface HasZero<S extends HasZero<S>>
	extends Eq<S> {

	/**
	 * For additive monoids and small categories, returns the Additive Neutral Element, which is
	 * called zero.
	 */
	S getZero();

	/**
	 * Returns true precisely when <code>this</code> is zero. In other words, we compute
	 * <code>this.eqv(getZero())</code>. Implementing classes could override this method if a better
	 * implementation is needed.
	 */
	default boolean isZero() {
		return this.eqv(getZero());
	}

}
