package xyz.mirq.structures;

import java.util.Optional;

/**
 * A <code>MultiplicativeGroupoid</code> is nearly a multiplicative group, but without requiring
 * that the combining operation be closed. Alternatively, it's a multiplicative small category with
 * reciprocals (multiplicative inverses) included. In other words, the <code>over</code> operation
 * returns an <code>Optional</code> instead of the <code>Self</code> type.
 */
public interface MultiplicativeGroupoid<S extends MultiplicativeGroupoid<S>>
	extends MultiplicativeSmallCategory<S>, Invertible<S> {

	/**
	 * Compute the <code>Optional</code> quotient <code>this / divisor</code>.
	 *
	 * @param divisor The divisor to divide into <code>this</code> dividend.
	 *
	 * @return <code>Optional(this / divisor)</code>
	 */
	default Optional<S> over(final S divisor) {
		/* Groupoid, therefore we flatMap, not map */
		return divisor.inverse().flatMap(this::times);
	}

	/**
	 * Compute the <code>Optional</code> product <code>this * that⁻¹</code>, or return
	 * <code>Optional.empty()</code> if <code>that</code>'s inverse does not exist.
	 *
	 * @param that The factor to be inverted before forming the product
	 *
	 * @return <code>Optional(this * that⁻¹)</code>
	 */
	default Optional<S> undo(S that) {
		/* Groupoid, therefore we flatMap, not map */
		return that.inverse().flatMap(this::times);
	}

}
