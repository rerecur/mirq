package xyz.mirq.structures;

import xyz.mirq.typeclasses.Eq;

/**
 * <code>HasUnit</code> is the shared functionality across <code>MultiplicativeMonoid</code> and
 * <code>MultiplicativeSmallCategory</code>. The methods defined here don't care whether the
 * multiplicative combine operation, <code>times</code>, is closed or not.
 *
 * @param <S> The <code>Self</code> type that inherits from <code>HasUnit&lt;S&gt;</code>.
 */
public interface HasUnit<S extends HasUnit<S>>
	extends Eq<S> {

	/**
	 * For multiplicative monoids and small categories, returns the Multiplicative Neutral Element,
	 * which is called unit, or one.
	 */
	S getUnit();

	/**
	 * Returns true precisely when <code>this</code> is one. In other words, we compute
	 * <code>this.eqv(getUnit())</code>. Implementing classes could override this method if a better
	 * implementation is needed.
	 */
	default boolean isUnit() {
		return this.eqv(getUnit());
	}

}
