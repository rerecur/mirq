package xyz.mirq.structures;

import java.util.Optional;

/**
 * <code>MultiplicativeGroup</code> elements form a multiplicative monoid where every element has a
 * multiplicative inverse, or reciprocal. This property is not enforced within the type system,
 * however. For example, the positive real numbers form a group.
 * <p>
 * <pre>
 *  class Foo implements MultiplicativeGroup&lt;Foo&gt; {
 *     double x;  // Not exactly the reals, but approximately so.
 *
 *     &#064;Override
 *     public Optional&lt;Foo&gt; inverse() {
 *         if (this.x == 0.0) {
 *             return Optional.empty();
 *         }
 *         final Foo foo = new Foo();
 *         foo.x = 1.0 / this.x;
 *         return Optional.of(foo);
 *     }
 *
 *     &#064;Override
 *     public Foo getUnit() {
 *         final Foo unit = new Foo();
 *         zero.x = 1.0;
 *         return unit;
 *     }
 *
 *     &#064;Override
 *     public Foo times(final Foo multiplicand) {
 *         final Foo product = new Foo();
 *         product.x = this.x * multiplicand.x;
 *         return product;
 *     }
 *
 *     &#064;Override
 *     public boolean eqv(final Foo that) {
 *         return this.x == that.x;
 *     }
 *
 *     &#064;Override
 *     public Foo self() {
 *         return this;
 *     }
 *  }
 * </pre>
 * </p>
 * @param <S> The <code>Self</code> type that inherits from
 *  <code>MultiplicativeGroup&lt;S&gt;</code>.
 */
public interface MultiplicativeGroup<S extends MultiplicativeGroup<S>>
	extends MultiplicativeMonoid<S>, Invertible<S> {

	/**
	 * Compute the quotient <code>this / divisor</code>. For a true group, this quotient always
	 * exists and this method will not throw <code>ArithmeticException</code>. But if this type is
	 * used to describe nearly groups, such as fields that have zeros, then this method could throw
	 * to indicate that the quotient does not exist.
	 *
	 * @param divisor The divisor to divide into <code>this</code> dividend.
	 *
	 * @return <code>this / divisor</code>
	 *
	 * @throws ArithmeticException If this is not a true group, and the quotient does not exist.
	 */
	default S over(final S divisor) throws ArithmeticException {
		return this.times(divisor.reciprocal());
	}

	/**
	 * Compute the <code>Optional</code> product <code>this * that⁻¹</code>, or return
	 * <code>Optional.empty()</code> if <code>that</code>'s inverse does not exist.
	 *
	 * @param that The factor to be inverted before forming the product
	 *
	 * @return <code>Optional(this * that⁻¹)</code>
	 */
	default Optional<S> undo(S that) {
		/* Group, therefore we map, not flatMap */
		return that.inverse().map(this::times);
	}

}
