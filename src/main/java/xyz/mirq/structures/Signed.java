package xyz.mirq.structures;

/**
 * <code>Signed</code> elements have a <code>signum</code> function that tells whether its value is
 * positive, zero, or negative. We also have convenience methods like <code>isPositive</code>.
 *
 * @param <S> The <code>Self</code> type that inherits from <code>Signed&lt;S&gt;</code>.
 */
public interface Signed<S extends Signed<S>>
	extends HasZero<S> {

	boolean isPositive();

	boolean isNegative();

	int signum();

	S abs();

}
