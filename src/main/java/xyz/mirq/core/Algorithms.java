package xyz.mirq.core;

public class Algorithms {

	/** Utility class */
	private Algorithms() {
	}

	/**
	 * Returns the positive Greatest Common Factor (aka Greatest Common Divisor) of any two
	 * long integers, or zero if both arguments are zero.
	 *
	 * @param a Can be any sign
	 * @param b Can be any sign
	 *
	 * @return The positive GCF, or zero if both arguments are zero.
	 */
	public static long gcf(long a, long b) {
		while (0 != a) {
			final long temp = a;
			a = b % a;
			b = temp;
		}
		return Math.abs(b);
	}

	/**
	 * Given a positive integer n, compute its square component m satisfying n = m²k, where k is the
	 * largest square-free divisor of n. By definition, the <strong>square component</strong> of an
	 * integer is the largest value whose square divides the integer. We return an array containing
	 * m and k, respectively.
	 * <p>
	 * Examples of square free numbers include 1, 2, 3, 5, 6, 7, 10.
	 * </p>
	 * <p>
	 * Examples athat are <strong>not</strong> square free include 0, 4, 8, 9, 12.
	 * </p>
	 *
	 * @param n Any positive integer
	 *
	 * @return [m, k] For <code>n = m²k</code>, where m is the square component, and k is the
	 * 	largest square-free divisor of n.
	 *
	 * @throws ArithmeticException If n is not positive
	 */
	public static long[] squareComponent(long n) throws ArithmeticException {
		if (n <= 0L) {
			throw new ArithmeticException("squareComponent(" + n + ")");
		}

		long m = 1L; // square component
		long k = 1L; // largest square-free divisor

		/* Let p be the next prime divisor to try */
		final LikelyPrimeIterator primes = new LikelyPrimeIterator();
		long p = primes.nextLong();
		long pᒾ = p * p;

		/* Loop until there are no more divisors to try */
		while (pᒾ <= n) {

			/* Increase m while pᒾ divides n */
			while (0 == n % pᒾ) {
				m *= p;
				n /= pᒾ;
			}

			/* Increase k if p divides n */
			if (0 == n % p) {
				k *= p;
				n /= p;
			}

			/* Determine the next prime(ish) divisor p */
			p = primes.nextLong();
			pᒾ = p * p;
		}

		k *= n;
		return new long[] { m, k };
	}

}
