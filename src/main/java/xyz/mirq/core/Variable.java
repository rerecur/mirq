package xyz.mirq.core;

import java.util.Objects;

import xyz.mirq.typeclasses.Eq;
import xyz.mirq.typeclasses.Order;

/**
 * A type-safe string that represents the name of a variable. Note that the <code>eqv</code> method
 * tests whether the variables are the same variable (have the same name), not whether they are
 * bound to the same value. Similarly, the natural ordering of variables is a lexicographic order,
 * not numerical value.
 */
public class Variable
	implements Eq<Variable>, Order<Variable> {

	public final String name;

	private Variable(String name) {
		this.name = name;
	}

	public static Variable of(String name) {
		return new Variable(name);
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public Variable self() {
		return this;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Variable)) {
			return false;
		}
		return this.eqv((Variable) o);
	}

	@Override
	public boolean eqv(Variable that) {
		return this.name.equals(that.name);
	}

	@Override
	public boolean lt(Variable that) {
		return this.name.compareTo(that.name) < 0;
	}
}
