package xyz.mirq.core;

/**
 * Useful utilities using Unicode.
 */
public class Unicode {

	/** Utility class */
	private Unicode() {
	}

	/** U+28, ASCII Open Parenthesis, aka Unicode Left Parenthesis, Basic Latin Block */
	public static final char LEFT_PARENTHESIS = '(';

	/** U+207D, Unicode Superscript Left Parenthesis */
	public static final char SUPERSCRIPT_LEFT_PARENTHESIS = '⁽';

	/** U+29, ASCII Close Parenthesis, aka Unicode Right Parenthesis, Basic Latin Block */
	public static final char RIGHT_PARENTHESIS = ')';

	/** U+207E, Unicode Superscript Right Parenthesis */
	public static final char SUPERSCRIPT_RIGHT_PARENTHESIS = '⁾';

	/** U+2B, ASCII Plus Sign, aka Unicode Plus Sign, Basic Latin Block */
	public static final char PLUS_SIGN = '+';

	/** U+207A, Superscript Plus Sign, Superscripts and Subscripts Block */
	public static final char SUPERSCRIPT_PLUS_SIGN = '⁺';

	/** U+2D, ASCII hyphen, aka Unicode Hyphen-Minus, Basic Latin Block */
	public static final char HYPHEN_MINUS = '-';

	/** U+207B, Superscript Minus, Superscripts and Subscripts Block */
	public static final char SUPERSCRIPT_MINUS = '⁻';

	/** U+2F, ASCII Slash, aka Unicode SOLIDUS, Basic Latin Block */
	public static final char SOLIDUS = '/';

	/** U+230D, Right Raised Omission Bracket, Supplemental Punctuation Block */
	public static final char RIGHT_RAISED_OMISSION_BRACKET = '⸍';

	/** U+2070, Superscript Zero, Superscripts and Subscripts Block */
	public static final char SUPERSCRIPT_ZERO = '⁰';

	/** U+69, ASCII Lowercase I, aka Latin Small Letter I, Basic Latin Block */
	public static final char LATIN_SMALL_LETTER_I = 'i';

	/** U+2071, Superscript Latin Small Letter I, Superscripts and Subscripts Block */
	public static final char SUPERSCRIPT_LATIN_SMALL_LETTER_I = 'ⁱ';

	/** U+B9, Superscript One, Latin-1 Supplement Block */
	public static final char SUPERSCRIPT_ONE = '¹';

	/** U+B2, Superscript Two, Latin-1 Supplement Block */
	public static final char SUPERSCRIPT_TWO = '²';

	/** U+B3, Superscript Three, Latin-1 Supplement Block */
	public static final char SUPERSCRIPT_THREE = '³';

	/**
	 * U+2074, Superscript Four, Superscripts and Subscripts Block. Note that the superscript digits
	 * greater than 4 are contiguous after this one. So, Superscript Five is U+2075, and so on.
	 */
	public static final char SUPERSCRIPT_FOUR = '⁴';

	/** U+2079, Superscript Nine, Superscripts and Subscripts Block */
	public static final char SUPERSCRIPT_NINE = '⁹';

	/**
	 * Compute a Unicode <code>String</code>representation of the given integer as superscript
	 * characters. So, for example, <code>superscript(-1)</code> returns "⁻¹".
	 *
	 * @param n The number to convert to Unicode superscript characters.
	 *
	 * @return Unicode superscript stringification of the number.
	 */
	public static String superscript(int n) {
		final StringBuilder builder = new StringBuilder();
		for (char c : Integer.toString(n).toCharArray()) {
			switch (c) {
				case HYPHEN_MINUS:
					builder.append(SUPERSCRIPT_MINUS);
					break;
				case '0':
					builder.append(SUPERSCRIPT_ZERO);
					break;
				case '1':
					builder.append(SUPERSCRIPT_ONE);
					break;
				case '2':
					builder.append(SUPERSCRIPT_TWO);
					break;
				case '3':
					builder.append(SUPERSCRIPT_THREE);
					break;
				default:
					/* Remaining digits are contiguous in Unicode */
					int code = c - '4' + SUPERSCRIPT_FOUR;
					builder.append((char) code);
			}
		}
		return builder.toString();
	}

}
