package xyz.mirq.core;

import java.util.PrimitiveIterator;

/**
 * A class to generate an ordered sequence of candidate prime numbers. The list is guaranteed not to
 * skip any authentic primes, but it might include some odd numbers that are not prime. This can be
 * helpful when factoring integers that are not "too large". Note that the <code>hasNext</code>
 * method always returns true.
 * <p>
 * Example Usage:
 * <pre>
 *     final LikelyPrimeGenerator primes = new LikelyPrimeGenerator();
 *     while ( ... some condition ... ) {
 *         final long p = primes.nextLong();
 *         ... do something ...
 *     }
 * </pre>
 * </p>
 */
public class LikelyPrimeIterator
	implements PrimitiveIterator.OfLong {

	/*
	 * This array must contain at least 2L and 3L for the algorithm to work.
	 */
	private static final long[] primes =
		{ 2L, 3L, 5L, 7L, 11L, 13L, 17L, 19L, 23L, 29L };

	private int index = 0;
	private long p;

	@Override
	public boolean hasNext() {
		return true;
	}

	@Override
	public long nextLong() {
		if (index < primes.length) {
			p = primes[index++];
		} else {
			p += 2L;
		}
		return p;
	}

}
