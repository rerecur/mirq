package xyz.mirq.radicals;

import xyz.mirq.quotients.QQ;
import xyz.mirq.structures.AdditiveGroupoid;
import xyz.mirq.structures.MultiplicativeGroup;
import xyz.mirq.structures.Signed;
import xyz.mirq.typeclasses.Order;

import java.util.Objects;
import java.util.Optional;

import static xyz.mirq.core.Algorithms.squareComponent;

/**
 * A <strong>real</strong> <code>Surd</code> is the square root of some non-negative rational
 * number, expressed in the form <code>q√(r)</code>, with rational coefficient <code>q</code> and
 * radicand <code>r</code>, where r is a positive square free integer. Note that if the radicand is
 * one, then our <code>Surd</code> is rational. And whenever the coefficient is zero, we define the
 * radicand to be one for convenience.
 * <p>
 * Multiplication of <code>Surd</code>s is closed, and every surd except zero has a reciprocal, so
 * we say they form a multiplicative group. (Although computing the reciprocal of zero will throw
 * <code>ArithmeticException</code>. However, if a pair of surds is non-zero and has different
 * radicands, then there's not way to simplify their sum into the form above. So, <code>Surd</code>s
 * comprise an additive groupoid, whose plus method returns an <code>Optional</code>.
 * </p>
 */
public class Surd
	implements MultiplicativeGroup<Surd>, AdditiveGroupoid<Surd>, Order<Surd>, Signed<Surd> {

	/**
	 * Defines String representations of surds of the form "rational√radicand".
	 */
	public static final String DELIMITER = "√";

	/**
	 * Additive Neutral Element.
	 */
	public static final Surd ZERO = new Surd(QQ.ZERO, 1);

	/**
	 * Multiplicative Neutral Element.
	 */
	public static final Surd UNIT = new Surd(QQ.UNIT, 1);

	/*-
		The radicand r is always positive square free. When the
		coefficient q is zero, then r is 1 for convenience.
	 */
	private final QQ q;
	private final long r;

	/**
	 * The rational coefficient of this irrational number, when expressed in simplest form. Note
	 * that this value need not be equal to the coefficient argument passed to the <code>of</code>
	 * constructor, because we reduce this expression to simplest form.
	 */
	public QQ getCoefficient() {
		return q;
	}

	/**
	 * The positive square free radicand of this irrational number, when expressed in simplest
	 * form. Note tht this value need not be equal to the radicand argument passed into the
	 * <code>of</code> constructor, because we reduce this expression to simplest form.
	 */
	public long getRadicand() {
		return r;
	}

	private Surd(QQ q, long r) {
		this.q = q;
		this.r = r;
	}

	/**
	 * Construct a real <code>Surd</code> of the form <code>(coefficient)√1</code>.
	 */
	public static Surd of(QQ coefficient) {
		return new Surd(coefficient, 1);
	}

	/**
	 * Construct a real <code>Surd</code> of the form <code>(coefficient)√1</code>.
	 */
	public static Surd of(long coefficient) {
		return new Surd(QQ.of(coefficient), 1);
	}

	/**
	 * Construct a real <code>Surd</code> of the form <code>(coefficient)√(radicand)</code>, or
	 * throw <code>ArithmeticException</code> if the result would be imaginary.
	 *
	 * @param coefficient Any rational number
	 * @param radicand Should be non-negative, unless the coefficient is zero
	 *
	 * @return <code>(coefficient)√(radicand)</code>, reduced to simplest form
	 *
	 * @throws ArithmeticException If the computation produces an imaginary number
	 */
	public static Surd of(QQ coefficient, long radicand) throws ArithmeticException {

		/* Ignore negative radicands when the coefficient is zero */
		if (radicand == 0L || coefficient.isZero()) {
			return ZERO;
		}

		/* In all other cases, negative radicands are not allowed */
		if (radicand < 0L) {
			throw new ArithmeticException("sqrt of " + radicand);
		}

		/*-
		 * let m and k be the square component and greatest square-free
		 * divisor of positive integer r, respectively. then...
		 *        _       _____          _
		 *      q√r  =  q√(mᒾk)  =  (qm)√k
		 *
		 */
		long[] m_k = squareComponent(radicand);
		return new Surd(coefficient.times(QQ.of(m_k[0])), m_k[1]);
	}

	/**
	 * Construct a real <code>Surd</code> of the form <code>(coefficient)√(radicand)</code>, or
	 * throw <code>ArithmeticException</code> if the result would be imaginary.
	 *
	 * @param coefficient Any rational number
	 * @param radicand Should be non-negative, unless the coefficient is zero
	 *
	 * @return <code>(coefficient)√(radicand)</code>, reduced to simplest form
	 *
	 * @throws ArithmeticException If the computation produces an imaginary number
	 */
	public static Surd of(QQ coefficient, QQ radicand) throws ArithmeticException {
		/*-
		 *      When b is known to be positive,
		 *       _____      _____      _____               ____
		 *      √(a/b)  =  √(a/b)  *  √(b/b)  = (1/b)  *  √(ab)
		 */
		return of(coefficient.over(radicand.getDenominator()),
			radicand.getNumerator() * radicand.getDenominator());
	}

	/**
	 * Construct a real <code>Surd</code> of the form <code>(coefficient)√(radicand)</code>, or
	 * throw <code>ArithmeticException</code> if the result would be imaginary. Note that this
	 * method offers a convenient way to compute the square root of any non-negative rational
	 * number. Simply pass one as the coefficient, for example...
	 * <pre>
	 *     final Surd value = // details elided
	 *     final Surd squareRootOfValue = Surd.of(1, value);
	 * </pre>
	 *
	 * @param coefficient Any integer
	 * @param radicand Should be non-negative, unless the coefficient is zero
	 *
	 * @return <code>(coefficient)√(radicand)</code>, reduced to simplest form
	 *
	 * @throws ArithmeticException If the computation produces an imaginary number
	 */
	public static Surd of(long coefficient, long radicand) throws ArithmeticException {
		return of(QQ.of(coefficient), QQ.of(radicand));
	}

	/**
	 * Construct a real <code>Surd</code> of the form <code>(coefficient)√(radicand)</code>, or
	 * throw <code>ArithmeticException</code> if the result would be imaginary. Note that this
	 * method offers a convenient way to compute the square root of any non-negative rational
	 * number. Simply pass one as the coefficient, for example...
	 * <pre>
	 *     final Surd value = // details elided
	 *     final Surd squareRootOfValue = Surd.of(1, value);
	 * </pre>
	 *
	 * @param coefficient Any integer
	 * @param radicand Should be non-negative, unless the coefficient is zero
	 *
	 * @return <code>(coefficient)√(radicand)</code>, reduced to simplest form
	 *
	 * @throws ArithmeticException If the computation produces an imaginary number
	 */
	public static Surd of(long coefficient, QQ radicand) throws ArithmeticException {
		return of(QQ.of(coefficient), radicand);
	}

	@Override
	public Surd self() {
		return this;
	}

	@Override
	public String toString() {
		if (isRational()) {
			/* No radical, "(42//5)" */
			return q.toString();
		} else if (QQ.UNIT.eqv(q)) {
			/* No explicit coefficient, "√2" */
			return "√" + String.valueOf(r);
		} else {
			/* Fully general, "(42//5)√2" */
			return q.toString() + "√" + String.valueOf(r);
		}
	}

	public static Surd parse(final String value) throws NumberFormatException {
		final String[] token = value.trim().split(DELIMITER, 2);
		if (token.length < 2) {
			return of(QQ.parse(token[0]));
		}

		final QQ coefficient = token[0].isEmpty() ? QQ.UNIT : QQ.parse(token[0]);
		final long radicand = Long.parseLong(token[1]);
		return of(coefficient, radicand);
	}

	@Override
	public int hashCode() {
		return Objects.hash(q, r);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Surd)) {
			return false;
		}
		return this.eqv((Surd) o);
	}

	@Override
	public boolean eqv(Surd that) {
		return this.q.eqv(that.q) && this.r == that.r;
	}

	@Override
	public boolean lt(Surd that) {
		final int thisSign = this.q.signum();
		final int thatSign = that.q.signum();
		if (thisSign == thatSign) {
			/* Then square both sides and compare them */
			final QQ thisᒾ = this.times(this).getCoefficient();
			final QQ thatᒾ = that.times(that).getCoefficient();
			if (thisSign < 0) {
				/* Then we flipped the relation when we squared */
				return thatᒾ.lt(thisᒾ);
			} else {
				return thisᒾ.lt(thatᒾ);
			}
		} else {
			return thisSign < thatSign;
		}
	}

	@Override
	public Surd getZero() {
		return ZERO;
	}

	@Override
	public boolean isPositive() {
		return q.isPositive();
	}

	@Override
	public boolean isNegative() {
		return q.isNegative();
	}

	@Override
	public int signum() {
		return Integer.signum(compareTo(ZERO));
	}

	@Override
	public Surd abs() {
		return new Surd(this.q.abs(), this.r);
	}

	public boolean isReal() {
		return true;
	}

	public boolean isRational() {
		return 1L == r;
	}

	public Surd conjugate() {
		if (this.isRational()) {
			return this;
		} else {
			return this.opposite();
		}
	}

	@Override
	public Surd times(Surd that) {
		/*-
		 *      When a and b are non-negative, it's always true that
		 *       _      _      __
		 *      √a  *  √b  =  √ab
		 */
		final QQ coefficient = this.q.times(that.q);
		return of(coefficient, this.r * that.r);
	}

	@Override
	public Surd getUnit() {
		return UNIT;
	}

	@Override
	public Optional<Surd> inverse() {
		/*-
		 *      When r is positive, it's always true that
		 *          _        _  _        _        _                 _
		 *      (1/√r)  =  (√r/√r) * (1/√r)  =  (√r/r)  =  (1/r) * √r
		 *
		 *      and therefore...
		 *            _                  _                       _               _
		 *      (1/(q√r)  =  (1/q) * (1/√r)  =  (1/q) * (1/r) * √r  =  (1/qr) * √r
		 */
		final Optional<QQ> coefficient = q.times(r).inverse();
		return coefficient.map(c -> new Surd(c, r));
	}

	@Override
	public Optional<Surd> plus(Surd that) {
		/* Zero is always the additive neutral element */
		if (this.isZero()) {
			return Optional.of(that);
		} else if (that.isZero()) {
			return Optional.of(this);
		}

		/* Otherwise, only terms with identical radicands can be added */
		if (this.r != that.r) {
			return Optional.empty();
		}

		/*-
		 *         _      _             _
		 *      q₁√r + q₂√r = (q₁ + q₂)√r
		 *
		 */
		return Optional.of(Surd.of(this.q.plus(that.q), r));
	}

	@Override
	public Surd opposite() {
		return new Surd(this.q.opposite(), this.r);
	}

}
