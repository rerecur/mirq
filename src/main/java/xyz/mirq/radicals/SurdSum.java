package xyz.mirq.radicals;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import xyz.mirq.quotients.QQ;
import xyz.mirq.structures.AdditiveGroup;

/**
 * A <strong>real</strong> <code>SurdSum</code> is an algebraic number of the form  ∑cᵢ√nᵢ, where nᵢ
 * are distinct positive square-free integers, and cᵢ are rational coefficients. In other words,
 * they are sums of <code>Surd</code>s, forming an ordered field. For example, a
 * <code>SurdSum</code> instance might be written
 * <pre>
 *            _     _
 *     10 + 2√2 + 5√6
 *
 * </pre>
 * where each summand is a <code>Surd</code> whose radicand is a positive square-free integer.
 */
public class SurdSum
	implements AdditiveGroup<SurdSum> {

	/* Delimiter used when converting to strings. */
	private static final String DELIMITER = " + ";

	/* Regex used when parsing from strings. */
	private static final String REGEX = "\\+";

	/** Additive Neutral Element, 0. */
	public static final SurdSum ZERO = new SurdSum(QQ.ZERO, Collections.emptySet());

	/** Multiplicative Neutral Element, 1. */
	public static final SurdSum UNIT = new SurdSum(QQ.UNIT, Collections.emptySet());

	/**
	 * Collection of summands reduced to lowest terms, meaning there are no zeros or multiple terms
	 * with matching radicands.
	 */
	private final Set<Surd> summands;

	private SurdSum(QQ q, Collection<Surd> summands) {

		/* Sort our summands by radicand */
		this.summands = new TreeSet<>(Comparator.comparing(Surd::getRadicand));

		/* Mutable Map, indexed by radicands, to combine like terms */
		final Map<Long, Surd> theMap = new HashMap<>();
		if (!q.isZero()) {
			Surd rationalPart = Surd.of(q);
			theMap.put(rationalPart.getRadicand(), rationalPart);
		}

		/*
		 * Combine all terms with like radicands. Note that merge will remove the
		 * entry from the Map if the value supplied is null. So it's good to return
		 * null instead of zero, to prune spurious terms.
		 */
		summands.stream().
			filter(s -> !s.isZero()).
			forEach(s -> theMap.merge(s.getRadicand(), s, SurdSum::sumOrNullOnZero));

		/* Copy simplified collection of terms into our set */
		this.summands.addAll(theMap.values());
	}

	/**
	 * Add the two <code>Surd</code>s together, or return null when the result would be zero. This
	 * is a deliberate use of null as a signal value to the <code>Map.merge</code> method. The
	 * burden falls on the caller to ensure that the radicands match, or else we risk
	 * <code>NoSuchElementException</code>
	 */
	private static Surd sumOrNullOnZero(Surd a, Surd b) {
		final Surd sum = a.plus(b).orElse(a);
		return sum.isZero() ? null : sum;
	}

	public static SurdSum of(QQ q, Surd... summands) {
		return new SurdSum(q, Arrays.asList(summands));
	}

	public static SurdSum of(long n, Surd... summands) {
		return of(QQ.of(n), summands);
	}

	public static SurdSum of(Surd... summands) {
		return of(QQ.ZERO, summands);
	}

	public static SurdSum of(Collection<Surd> summands) {
		return new SurdSum(QQ.ZERO, summands);
	}

	@Override
	public String toString() {
		if (summands.isEmpty()) {
			return "0";
		} else {
			return summands.stream().
				map(Object::toString).
				collect(Collectors.joining(DELIMITER));
		}
	}

	/**
	 * Parse a String containing the <code>toString</code> form of the surd sum.
	 *
	 * @throws NumberFormatException If the string is not properly formatted.
	 */
	public static SurdSum parse(String value) throws NumberFormatException {
		return of(Arrays.stream(value.trim().split(REGEX)).
			map(String::trim).
			map(Surd::parse).
			collect(Collectors.toList()));
	}

	@Override
	public SurdSum self() {
		return this;
	}

	@Override
	public int hashCode() {
		return Objects.hash(
			summands.stream().
				map(Surd::hashCode).
				toArray(Object[]::new)
		);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof SurdSum)) {
			return false;
		}
		return this.eqv((SurdSum) o);
	}

	@Override
	public boolean eqv(SurdSum that) {
		return this.summands.equals(that.summands);
	}

	@Override
	public SurdSum plus(SurdSum that) {
		return of(Stream.concat(this.summands.stream(), that.summands.stream()).
			collect(Collectors.toList()));
	}

	public SurdSum plus(Surd that) {
		return this.plus(of(that));
	}

	public SurdSum minus(Surd that) {
		return this.minus(of(that));
	}

	public SurdSum plus(QQ that) {
		return this.plus(of(that));
	}

	public SurdSum minus(QQ that) {
		return this.minus(of(that));
	}

	public SurdSum plus(long that) {
		return this.plus(of(that));
	}

	public SurdSum minus(long that) {
		return this.minus(of(that));
	}

	@Override
	public SurdSum getZero() {
		return ZERO;
	}

	@Override
	public SurdSum opposite() {
		return of(summands.stream().
			map(Surd::opposite).
			collect(Collectors.toList()));
	}

	/**
	 * The <code>conjugate</code> of a <code>SurdSum</code> is the sum of its rational part and the
	 * opposites of all its irrational summands. In other words, the conjugate of
	 * <pre>q + ∑cᵢ√nᵢ</pre> is <pre>q - ∑cᵢ√nᵢ</pre>. The <code>conjugate</code> method is useful
	 * when constructing the reciprocal of a surd sum.
	 *
	 * @return q - ∑cᵢ√nᵢ
	 */
	public SurdSum conjugate() {
		return of(summands.stream().
			map(Surd::conjugate).
			collect(Collectors.toList()));
	}

	/**
	 * The <code>normL1</code> of a <code>SurdSum</code> is the sum of the absolute values of all of
	 * its summands. This is analogous to the ℓ1-norm of a vector, if we consider each radicand as a
	 * basis. Specifically, the ℓ1-norm of <pre>q + ∑cᵢ√nᵢ</pre> is <pre>|q| + ∑|cᵢ|√nᵢ</pre>,
	 * which is guaranteed non-negative. The <code>normL1</code> method is useful when determining
	 * the sign of a surd sum.
	 *
	 * @return |q| + ∑|cᵢ|√nᵢ
	 */
	public SurdSum normL1() {
		return of(summands.stream().
			map(Surd::abs).
			collect(Collectors.toList()));
	}

}
