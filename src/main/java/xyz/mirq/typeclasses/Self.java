package xyz.mirq.typeclasses;

/**
 * <code>Self</code> classes permit safe casts of type parameters to one's own type. This allows
 * parameterized interfaces to "know" their implementing class. This can be useful when swapping
 * the operands of a non-commutative binary operation, like matrix multiplication.
 * <p>
 * Example Usage:
 * <pre>
 * class Foo implements Self&lt;Foo&gt; {
 *     &#064;Override
 *     public Foo self() {
 *         return this;
 *     }
 * }
 * </pre>
 * </p>
 *
 * @param <S> One's own type, to cast to.
 */
public interface Self<S> {

	/**
	 * @return <code>this</code>, which is of type S.
	 */
	S self();
}
