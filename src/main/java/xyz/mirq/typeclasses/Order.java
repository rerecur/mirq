package xyz.mirq.typeclasses;

/**
 * <code>Order</code> is the typeclass analog of Java's <code>Comparable</code>, but
 * with friendlier, more readable methods. Note that <code>Order</code> does
 * <strong>not</strong> extend <code>Eq</code>, because there are types like floating
 * point numbers that have a natural ordering, but for which equality is not to be trusted. However
 * most types that implement <code>Order</code> will also implement
 * </code>Eq</code>.
 * <p>
 * Example Usage:
 * <pre>
 *  class Foo implements Order&lt;Foo&gt; {
 *     int n;
 *
 *     &#064;Override
 *     public boolean lt(final Foo addend) {
 *         return this.n < that.n;
 *     }
 *
 *     &#064;Override
 *     public Foo self() {
 *         return this;
 *     }
 *  }
 * </pre>
 *
 * @param <S> The <code>Self</code> type that inherits from <code>Order&lt;S&gt;</code>.
 */
public interface Order<S extends Order<S>>
	extends Self<S>, Comparable<S> {

	/**
	 * Less Than operator, computes <code>this &lt; that</code>.
	 */
	boolean lt(final S that);

	/**
	 * Greater Than operator, computes <code>this &gt; that</code>.
	 */
	default boolean gt(final S that) {
		return that.lt(this.self());
	}

	/**
	 * Less Than Or Equal To operator, computes <code>this &lt;= that</code>.
	 */
	default boolean lte(final S that) {
		return !this.gt(that);
	}

	/**
	 * Greater Than Or Equal To operator, computes <code>this &gt;= that</code>.
	 */
	default boolean gte(final S that) {
		return !this.lt(that);
	}

	@Override
	default int compareTo(final S that) {
		if (this.lt(that)) {
			return -1;
		} else if (this.gt(that)) {
			return +1;
		} else {
			return 0;
		}
	}

}
