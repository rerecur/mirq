package xyz.mirq.typeclasses;

/**
 * <code>Eq</code> is the typeclass analog of Java's
 * <code>Object.equals</code>, but with added type safety.
 * <p>
 * Example Usage:
 * <pre>
 * class Foo implements Eq&lt;Foo&gt; {
 *     int n;
 *
 *     &#064;Override
 *     public boolean eqv(final Foo that) {
 *         return this.n == that.n;
 *     }
 *
 *     &#064;Override
 *     public Foo self() {
 *         return this;
 *     }
 * }
 * </pre>
 *
 * @param <S> The <code>Self</code> type that inherits from <code>Eq&lt;S&gt;</code>.
 */
public interface Eq<S extends Eq<S>>
	extends Self<S> {

	/**
	 * Returns whether two objects of type <code>S</code> are equivalent.
	 */
	boolean eqv(final S that);

	/**
	 * Logical negation of <code>eqv</code> method.
	 */
	default boolean neq(final S that) {
		return !this.eqv(that);
	}
}
