package xyz.mirq.quotients;

import java.util.Objects;
import java.util.Optional;

import xyz.mirq.core.Variable;
import xyz.mirq.structures.MultiplicativeGroupoid;

import static xyz.mirq.core.Unicode.superscript;

/**
 * A <code>Power</code> represents some <code>Variable</code> raised to an integral power, such as
 * <pre>
 *      y, x², x⁰ == 1, n⁻¹ == n̅
 * </pre>
 * and so on. <code>Power</code>s form a multiplicative groupoid, because the product of two
 * non-zero degree powers of the same variable is also a power. For example x * x² = x³, which is a
 * <code>Power</code>. But x * y is not a power (although it is a monomial).
 */
public class Power
	implements MultiplicativeGroupoid<Power> {

	public final Variable variable;
	public final int degree;

	private Power(Variable variable, int degree) {
		this.variable = variable;
		this.degree = degree;
	}

	public static Power of(Variable variable, int degree) {
		return new Power(variable, degree);
	}

	public static Power of(Variable variable) {
		return new Power(variable, 1);
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder(variable.toString());
		if (degree != 1) {
			builder.append(superscript(degree));
		}
		return builder.toString();
	}

	@Override
	public Power self() {
		return this;
	}

	@Override
	public int hashCode() {
		return degree == 0 ?
			Objects.hash(degree) :
			Objects.hash(variable, degree);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Power)) {
			return false;
		}
		return this.eqv((Power) o);
	}

	@Override
	public boolean eqv(Power that) {
		return
			this.degree == that.degree &&
			(0 == degree || this.variable.eqv(that.variable));
	}

	public Variable getVariable() {
		return variable;
	}

	public int getDegree() {
		return degree;
	}

	@Override
	public Optional<Power> times(Power that) {
		if (this.isUnit()) {
			return Optional.of(that);
		} else if (that.isUnit()) {
			return Optional.of(this);
		}

		if (this.variable.eqv(that.variable)) {
			return Optional.of(of(variable, this.degree + that.degree));
		} else {
			return Optional.empty();
		}
	}

	@Override
	public Power getUnit() {
		return of(variable, 0);
	}

	@Override
	public Optional<Power> inverse() {
		return Optional.of(isUnit() ? this : of(variable, -degree));
	}

}
