package xyz.mirq.quotients;

import xyz.mirq.structures.OrderedField;

import java.util.Objects;
import java.util.Optional;

import static xyz.mirq.core.Algorithms.gcf;

/**
 * Rational numbers of the form "numerator//denominator", where "numerator" is any integer,
 * "denominator" is a positive integer, and they are relatively prime. In the special case where the
 * numerator is zero, the denominator is simply one. Any expression where the denominator would be
 * zero, including "zero//zero", is not a rational number, and cannot be constructed.
 * <p>
 * Examples:
 * <pre>
 *  //  5//1
 *  QQ q = QQ.of(5);
 *
 *  //  22/7
 *  QQ pi = QQ.of(22).over(7);
 *
 *  //  57//7
 *  QQ sum = q + pi;
 * </pre>
 * </p>
 */
public class QQ
	implements OrderedField<QQ> {

	/**
	 * Defines String representations of rational numbers of the form, "numerator//denominator".
	 */
	public static final String DELIMITER = "//";

	/**
	 * Additive Neutral Element.
	 */
	public static final QQ ZERO = new QQ(0, 1);

	/**
	 * Multiplicative Neutral Element.
	 */
	public static final QQ UNIT = new QQ(1, 1);

	/*-
		The numerator and denominator are always relatively prime,
		and the denominator is always positive. When the numerator
		is zero, the denominator is one.
	 */
	private final long num;
	private final long den;

	/**
	 * The numerator of this rational number, when expressed in simplest form. In other words, the
	 * numerator and denominator are relatively prime, and the sign of the rational number is given
	 * by the sign of the numerator.
	 * <p>
	 * Note that this value need not be equal to the dividend argument passed to the
	 * <code>of</code> constructor, because <code>of</code> does not require its
	 * arguments to be relatively prime.
	 * </p>
	 */
	public long getNumerator() {
		return num;
	}

	/**
	 * The denominator of this rational number, when expressed in simplest form. In other words, the
	 * numerator and denominator are relatively prime, the denominator is always positive, and when
	 * the numerator is zero, the denominator is one.
	 * <p>
	 * Note that this value need not be equal to the divisor argument passed to the
	 * <code>of</code> constructor, because <code>of</code> does not require its
	 * arguments to be relatively prime.
	 * </p>
	 */
	public long getDenominator() {
		return den;
	}

	/*-
	 * Deliberately private! The caller must validate the
	 * arguments by guaranteeing that the denominator is
	 * positive and the numerator and denominator are
	 * relatively prime. Moreover, if the numerator is
	 * zero, then the denominator must be one.
	 */
	private QQ(final long num, final long den) {
		this.num = num;
		this.den = den;
	}

	/**
	 * Construct a rational number from the given integer, n, which can be of any sign. In other
	 * words, n//1 is the resulting rational number.
	 *
	 * @param n Can be any integer
	 *
	 * @return A rational number with numerator n and denominator 1
	 */
	public static QQ of(final long n) {
		return new QQ(n, 1);
	}

	/**
	 * Construct a rational number by forming the quotient = dividend ÷ divisor. These arguments can
	 * be any values except that the denominator must not be zero. Since this method is private, the
	 * burden to ensure non-zero divisors falls on the caller.
	 *
	 * @param dividend Any integer
	 * @param divisor Any integer except zero
	 *
	 * @return The quotient = dividend ÷ divisor
	 */
	private static QQ valueOf(final long dividend, final long divisor) throws ArithmeticException {
		if (0 == dividend) {
			return ZERO;
		}
		if (dividend == divisor) {
			return UNIT;
		}

		final long common = gcf(dividend, divisor);
		final boolean isQuotientNegative = dividend * divisor < 0;
		if (isQuotientNegative) {
			return new QQ(-Math.abs(dividend / common), Math.abs(divisor / common));
		} else {
			return new QQ(Math.abs(dividend / common), Math.abs(divisor / common));
		}
	}

	@Override
	public QQ self() {
		return this;
	}

	/**
	 * The unique String representation of this rational number, which uses a double slash "//" to
	 * separate the numerator and denominator in those cases when the denominator is not one.
	 * Negative numbers and non-integers are wrapped in parentheses.
	 */
	@Override
	public String toString() {
		if (1 == den) {
			if (num >= 0L) {
				/* Non-Negative Integer, "42" */
				return String.valueOf(num);
			} else {
				/* Negative Integer, "(-42)" */
				return "(" + String.valueOf(num) + ")";
			}
		} else {
			/* Non-Integer, "(42//5)" */
			return "(" + String.valueOf(num) + DELIMITER + String.valueOf(den) + ")";
		}
	}

	/**
	 * Parse a String containing the <code>toString</code> form of the rational number, which is
	 * either "integer", or "integer//positiveInteger". The integers need not be relatively prime,
	 * but the dividend must be positive if it is supplied.
	 * <p>
	 * Note that <code>toString</code> and <code>parse</code> are almost inverses. Given any
	 * rational number <code>q</code>, <code>q.eqv(QQ.parse(q.toString()))</code> is always true.
	 * However, when given a parseable string <code>s</code>, it's not necessarily true that
	 * <code>s.equals(QQ.parse(s).toString())</code>, because parseable rational number strings need
	 * not be relatively prime.
	 * </p>
	 *
	 * @param value A String of the form "number" or "integer//positiveInteger"
	 *
	 * @return The rational number defined in the string.
	 *
	 * @throws NumberFormatException If the string is not properly formatted.
	 */
	public static QQ parse(final String value) throws NumberFormatException {
		final String[] token = value.trim().split(DELIMITER, 2);
		if (token.length < 2) {
			final long dividend = token[0].startsWith("(") ?
				Long.parseLong(token[0].substring(1, token[0].length() - 1)) :
				Long.parseLong(token[0]);
			return of(dividend);
		}

		final long dividend = token[0].startsWith("(") ?
			Long.parseLong(token[0].substring(1)) :
			Long.parseLong(token[0]);
		final long divisor = token[1].endsWith(")") ?
			Long.parseLong(token[1].substring(0, token[1].length()-1)) :
			Long.parseLong(token[1]);
		if (divisor <= 0) {
			throw new NumberFormatException(value);
		}
		/* Since we made it this far, divisor cannot be zero */
		return valueOf(dividend, divisor);
	}

	@Override
	public int hashCode() {
		return Objects.hash(num, den);
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof QQ)) {
			return false;
		}
		return this.eqv((QQ) o);
	}

	@Override
	public boolean eqv(QQ that) {
		return this.num == that.num && this.den == that.den;
	}

	@Override
	public boolean lt(final QQ that) {
		return this.num * that.den < this.den * that.num;
	}

	@Override
	public QQ getZero() {
		return ZERO;
	}

	@Override
	public QQ plus(QQ that) {
		/*-
		 *		 a       c       ad + bc
		 *		---  +  ---  =  ---------
		 *		 b       d         bd
		 *
		 * The product of two positive denominators cannot be zero.
		 */
		return valueOf(
			this.num * that.den + this.den * that.num,
			this.den * that.den
		);
	}

	/**
	 * Convenience method to compute the sum of this rational number and the integer summand.
	 *
	 * @param summand Any integer
	 *
	 * @return <code>this + summand</code>
	 */
	public QQ plus(long summand) {
		return this.plus(of(summand));
	}

	@Override
	public QQ opposite() {
		return new QQ(-num, den);
	}

	/**
	 * Convenience method to compute the difference of this rational number and the integer
	 * subtrahend.
	 *
	 * @param subtrahend Any integer
	 *
	 * @return <code>this - subtrahend</code>
	 */
	public QQ minus(long subtrahend) {
		return this.minus(of(subtrahend));
	}

	@Override
	public QQ times(QQ that) {
		/*-
		 *		 a       c       ac
		 *		---  *  ---  =  ----
		 *		 b       d       bd
		 *
		 * The produce of two positive denominators cannot be zero.
		 */
		return valueOf(this.num * that.num, that.den * this.den);
	}

	/**
	 * Convenience method to compute the product of this rational number and the integer factor.
	 *
	 * @param factor Any integer
	 *
	 * @return <code>this * factor</code>
	 */
	public QQ times(long factor) {
		return this.times(of(factor));
	}

	@Override
	public QQ getUnit() {
		return UNIT;
	}

	/**
	 * Constructs the <code>Optional</code> multiplicative inverse, or returns empty if the
	 * reciprocal does not exist.
	 */
	@Override
	public Optional<QQ> inverse() {
		if (num == 0) {
			return Optional.empty();
		}

		/*
		 * We already know these are relatively prime, so
		 * we just have to worry about their signs.
		 */
		if (0 < num) {
			return Optional.of(new QQ(den, num));
		} else {
			return Optional.of(new QQ(-den, -num));
		}
	}

	/**
	 * Convenience method to compute the quotient of this rational number and the integer divisor,
	 * and throws <code>ArithmeticException</code> if the divisor is zero
	 *
	 * @param divisor Any integer except zero
	 *
	 * @return <code>this / divisor</code>
	 */
	public QQ over(long divisor) {
		return this.over(of(divisor));
	}

}
