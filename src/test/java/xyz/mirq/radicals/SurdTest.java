package xyz.mirq.radicals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import xyz.mirq.quotients.QQ;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SurdTest {

	@Test
	void testGetUnit() {
		final Surd a = Surd.of(3, 6);
		final Surd unit = a.getUnit();

		assertEquals(1L, unit.getRadicand());
		assertEquals(QQ.UNIT, unit.getCoefficient());
	}

	@Test
	void testTimes() {
		final Surd a = Surd.of(3, 6);
		final Surd b = Surd.of(5, 10);
		assertEquals(Surd.of(30, 15), a.times(b));
	}

	@Test
	void reciprocalOfZeroShouldThrow() {
		Assertions.assertThrows(ArithmeticException.class, () -> {
			Surd.ZERO.reciprocal();
		});
	}

	@Test
	void testReciprocal() {
		final Surd s = Surd.of(5, 6).reciprocal();
		assertEquals(Surd.of(QQ.of(1).over(30), 6), s);
	}

	@Test
	void testUndo() {
		final Optional<Surd> quotient = Surd.UNIT.undo(Surd.ZERO);
		assertFalse(quotient.isPresent());
	}

	@Test
	void testHashCode() {
		assertNotEquals(Surd.ZERO.hashCode(), Surd.UNIT.hashCode());
	}

	@Test
	@SuppressWarnings("squid:S5785") // Deliberately testing equals
	void testEqualsDifferentTypes() {
		assertFalse(Surd.UNIT.equals("1"));
	}

	@Test
	void testEqvDifferentRadicands() {
		final Surd fiveRootTwo = Surd.of(1, 50);
		final Surd fiveRootThree = Surd.of(1, 75);

		assertFalse(fiveRootTwo.eqv(fiveRootThree));
	}

	@Test
	void testIsReal() {
		assertTrue(Surd.UNIT.isReal());
	}

	@Test
	void testIsPositive() {
		assertTrue(Surd.UNIT.isPositive());
		assertFalse(Surd.ZERO.isPositive());
	}

}
