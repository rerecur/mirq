package xyz.mirq.radicals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SurdSumConstructorTest {

	static Surd five;
	static Surd threeRootTwo;
	static Surd rootSix;

	@BeforeAll
	static void beforeAll() {
		five = Surd.of(5);
		threeRootTwo = Surd.of(3, 2);
		rootSix = Surd.of(1, 6);
	}

	@Test
	void testZeroAndUnit() {
		assertEquals("0", SurdSum.ZERO.toString());
		assertEquals("1", SurdSum.UNIT.toString());
	}

	@Test
	void testFive() {
		assertEquals("5", SurdSum.of(5).toString());
		assertEquals("5", SurdSum.of(five).toString());
		assertEquals("5", SurdSum.of(0, five).toString());
		assertEquals("5", SurdSum.of(Surd.ZERO, five).toString());
	}

	@Test
	void testFivePlusOne() {
		assertEquals("6", SurdSum.of(5, Surd.UNIT).toString());
		assertEquals("6", SurdSum.of(five, Surd.UNIT).toString());
	}

	@Test
	void testOneMinusFive() {
		assertEquals("(-4)", SurdSum.of(1, five.opposite()).toString());
		assertEquals("(-4)", SurdSum.of(Surd.UNIT, five.opposite()).toString());
	}

	@Test
	void testThreeRootTwo() {
		assertEquals("3√2", SurdSum.of(threeRootTwo).toString());
	}

	@Test
	void testThreeRootTwoPlusFive() {
		assertEquals("5 + 3√2", SurdSum.of(threeRootTwo, five).toString());
	}

	@Test
	void testThreeRootTwoPlusFiveMinusThreeRootTwo() {
		assertEquals(
			"5",
			SurdSum.of(threeRootTwo, five, threeRootTwo.opposite()).toString());
	}

	@Test
	void testThreeRootTwoPlusThreeRootTwo() {
		assertEquals(
			"6√2",
			SurdSum.of(threeRootTwo, threeRootTwo).toString());
	}

	@Test
	void testTriplet() {
		final String expected = "5 + 3√2 + √6";
		assertEquals(
			expected,
			SurdSum.of(rootSix, five, threeRootTwo).toString()
		);

		final List<Surd> list = Arrays.asList(rootSix, five, threeRootTwo);
		assertEquals(expected, SurdSum.of(list).toString());
	}

}
