package xyz.mirq.radicals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import xyz.mirq.quotients.QQ;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SurdSumAdditiveTest {

	static QQ fiveHalves;
	static QQ twoThirds;
	static Surd twoRootThree;
	static Surd sevenRootThree;
	static Surd halfRootTwo;

	@BeforeAll
	static void beforeAll() {
		fiveHalves = QQ.of(5).over(2);
		twoThirds = QQ.of(2).over(3);
		twoRootThree = Surd.of(2, 3);
		sevenRootThree = Surd.of(7, 3);
		halfRootTwo = Surd.of(QQ.of(1).over(2), 2);
	}

	@Test
	void testPlusRationals() {
		final QQ expected = fiveHalves.plus(twoThirds);
		final SurdSum sum = SurdSum.of(fiveHalves).plus(SurdSum.of(twoThirds));

		assertEquals(expected.toString(), sum.toString());
	}

	@Test
	void testPlusLikeRadicands() {
		final Surd expected = twoRootThree.plus(sevenRootThree).get();
		final SurdSum sum = SurdSum.of(twoRootThree).plus(SurdSum.of(sevenRootThree));

		assertEquals(expected.toString(), sum.toString());
		assertEquals(expected.toString(), SurdSum.of(twoRootThree, sevenRootThree).toString());
	}

	@Test
	void testPlusUnlikeRadicands() {
		final String expected = "(1//2)√2 + 2√3";
		final SurdSum sum = SurdSum.of(twoRootThree).plus(SurdSum.of(halfRootTwo));

		assertEquals(expected, sum.toString());
		assertEquals(expected, SurdSum.of(twoRootThree, halfRootTwo).toString());
	}

	@Test
	void testIdenticalSurdAddends() {
		final String expected = "(5//2) + √2 + 2√3";
		final SurdSum augend = SurdSum.of(halfRootTwo).plus(SurdSum.of(fiveHalves));
		final SurdSum addend = SurdSum.of(halfRootTwo).plus(SurdSum.of(twoRootThree));
		final SurdSum sum = augend.plus(addend);

		assertEquals(expected, sum.toString());
		assertEquals(expected, augend.sumByAugend(addend).toString());
	}

	@Test
	void testPlusSurd() {
		final String expected = "(1//2)√2 + 9√3";
		final SurdSum sum = SurdSum.of(twoRootThree).plus(SurdSum.of(halfRootTwo));

		assertEquals(expected, sum.plus(sevenRootThree).toString());
	}

	@Test
	void testMinusSurd() {
		final String expected = "(1//2)√2 + (-5)√3";
		final SurdSum sum = SurdSum.of(twoRootThree).plus(SurdSum.of(halfRootTwo));

		assertEquals(expected, sum.minus(sevenRootThree).toString());
	}


	@Test
	void testPlusRational() {
		final String expected = "(2//3) + 2√3";
		final SurdSum sum = SurdSum.of(twoRootThree).plus(twoThirds);

		assertEquals(expected, sum.toString());
	}

	@Test
	void testMinusRational() {
		final String expected = "(-2//3) + 2√3";
		final SurdSum difference = SurdSum.of(twoRootThree).minus(twoThirds);

		assertEquals(expected, difference.toString());
	}

	@Test
	void testPlusLong() {
		final String expected = "(-4) + 2√3";
		final SurdSum sum = SurdSum.of(twoRootThree).plus(-4);

		assertEquals(expected, sum.toString());
	}

	@Test
	void testMinusLong() {
		final String expected = "(-28//3) + 2√3";
		final SurdSum sum = SurdSum.of(twoRootThree).plus(twoThirds);
		final SurdSum difference = sum.minus(10);

		assertEquals(expected, difference.toString());
	}

	@Test
	void testGetZero() {
		final SurdSum sum = SurdSum.of(halfRootTwo);
		assertTrue(SurdSum.ZERO.eqv(sum.getZero()));
		assertFalse(SurdSum.ZERO.eqv(sum));

		assertTrue(sum.getZero().isZero());
		assertFalse(sum.isZero());
	}

	@Test
	void testOpposite() {
		final String expected = "(-5//2) + (-1)√2 + 2√3";
		final SurdSum augend = SurdSum.of(halfRootTwo).plus(SurdSum.of(fiveHalves));
		final SurdSum addend = SurdSum.of(halfRootTwo).plus(SurdSum.of(twoRootThree.opposite()));
		final SurdSum sum = augend.plus(addend).opposite();

		assertEquals(expected, sum.toString());
		assertTrue(sum.minus(sum).isZero());
	}

	@Test
	void testOppositeOfZero() {
		assertTrue(SurdSum.ZERO.opposite().isZero());
	}

	@Test
	void testConjugate() {
		final String expected = "(5//2) + (1//2)√2 + (-2)√3";
		final SurdSum sum = SurdSum.of(fiveHalves, twoRootThree, halfRootTwo.opposite());

		assertEquals(expected, sum.conjugate().toString());
	}

	@Test
	void testConjugateOfZero() {
		assertTrue(SurdSum.ZERO.conjugate().isZero());
	}

	@Test
	void testNormL1() {
		final String expected = "(5//2) + (1//2)√2 + 2√3";
		final SurdSum sum = SurdSum.of(fiveHalves, twoRootThree, halfRootTwo.opposite());

		assertEquals(expected, sum.normL1().toString());
	}

	@Test
	void testNormL1OfZero() {
		assertTrue(SurdSum.ZERO.normL1().isZero());
	}

}
