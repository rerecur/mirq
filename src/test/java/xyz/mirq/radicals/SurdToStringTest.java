package xyz.mirq.radicals;

import org.junit.jupiter.api.Test;
import xyz.mirq.quotients.QQ;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SurdToStringTest {

	@Test
	void zeroShouldBe0() {
		assertEquals("0", Surd.ZERO.toString());
	}

	@Test
	void unitShouldBe1() {
		assertEquals("1", Surd.UNIT.toString());
	}

	@Test
	void testUnitCoefficient() {
		final Surd rootTwo = Surd.of(1, 2);
		final String s = "√2";

		assertEquals(s, rootTwo.toString());
		assertTrue(rootTwo.eqv(Surd.parse(s)));
	}

	@Test
	void testNonUnitCoefficient() {
		final Surd threeRootTwo = Surd.of(3, 2);
		final String s = "3√2";

		assertEquals(s, threeRootTwo.toString());
		assertTrue(threeRootTwo.eqv(Surd.parse(s)));
	}

	@Test
	void rationalShouldNotNeedDelimiter() {
		final Surd q = Surd.of(QQ.of(4).over(3));
		final String s = "(4//3)";

		assertEquals(s, q.toString());
		assertTrue(q.eqv(Surd.parse(s)));
	}

}
