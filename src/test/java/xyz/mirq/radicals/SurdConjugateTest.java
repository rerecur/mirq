package xyz.mirq.radicals;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SurdConjugateTest {

	@Test
	void rationalConjugateShouldBeNoOp() {
		final Surd q = Surd.of(5, 1);
		assertEquals(q, q.conjugate());
	}

	@Test
	void irrationalConjugateShouldNegate() {
		final Surd x = Surd.of(3, 2);
		assertEquals(Surd.of(-3, 2), x.conjugate());
	}

}
