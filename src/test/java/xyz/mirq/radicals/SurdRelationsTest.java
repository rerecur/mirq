package xyz.mirq.radicals;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SurdRelationsTest {

	@Test
	void testBigRadicand() {
		final Surd a = Surd.of(5, 5);
		final Surd b = Surd.of(2, 101);

		assertTrue(a.lt(b));
	}

	@Test
	void testNegatives() {
		final Surd a = Surd.of(-5, 5);
		final Surd b = Surd.of(-2, 101);

		assertFalse(a.lt(b));
	}

	@Test
	void testDifferentSignums() {
		final Surd neg = Surd.of(-5, 2);
		final Surd pos = Surd.of(3, 7);

		assertTrue(neg.lt(pos));
		assertFalse(pos.lt(neg));
	}

}
