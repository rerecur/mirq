package xyz.mirq.radicals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import xyz.mirq.quotients.QQ;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SurdConstructorTest {

	@Test
	void testOfLong() {
		assertTrue(Surd.ZERO.eqv(Surd.of(0)));
		assertTrue(Surd.UNIT.eqv(Surd.of(1)));

		final Surd r = Surd.of(50);
		assertEquals(QQ.of(50), r.getCoefficient());
		assertEquals(1L, r.getRadicand());
	}

	@Test
	void testOfRational() {
		assertTrue(Surd.ZERO.eqv(Surd.of(QQ.ZERO)));
		assertTrue(Surd.UNIT.eqv(Surd.of(QQ.UNIT)));

		final Surd r = Surd.of(QQ.of(50).over(3));
		assertEquals(QQ.of(50).over(3), r.getCoefficient());
		assertEquals(1L, r.getRadicand());
	}

	@Test
	void testOfZeroRadicand() {
		assertTrue(Surd.ZERO.eqv(Surd.of(QQ.UNIT, 0)));
		assertTrue(Surd.ZERO.eqv(Surd.of(QQ.UNIT, QQ.ZERO)));
	}

	@Test
	void testOfZeroCoefficient() {
		assertTrue(Surd.ZERO.eqv(Surd.of(QQ.ZERO, -1)));
		assertTrue(Surd.ZERO.eqv(Surd.of(QQ.ZERO, QQ.UNIT.opposite())));
	}

	@Test
	void testOfNegativeRadicand() {
		Assertions.assertThrows(ArithmeticException.class, () -> {
			Surd.of(QQ.UNIT, -1);
		});

		final QQ negativeOne = QQ.UNIT.opposite();
		Assertions.assertThrows(ArithmeticException.class, () -> {
			Surd.of(QQ.UNIT, negativeOne);
		});
	}

	@Test
	void testOfLongRadicand() {
		final Surd r = Surd.of(QQ.UNIT.over(3), 50);
		assertEquals(QQ.of(5).over(3), r.getCoefficient());
		assertEquals(2L, r.getRadicand());
	}

	@Test
	void testOfRationalRadicand() {
		final Surd r = Surd.of(QQ.UNIT.over(3), QQ.of(50).over(7));
		assertEquals(QQ.of(5).over(21), r.getCoefficient());
		assertEquals(14L, r.getRadicand());
	}

	@Test
	void testSquareRootRational() {
		final Surd r = Surd.of(1, QQ.of(18).over(175));
		assertEquals(QQ.of(3).over(35), r.getCoefficient());
		assertEquals(14L, r.getRadicand());
	}

	@Test
	void testIntegralCoefficientRationalRadicand() {
		final Surd r = Surd.of(11, QQ.of(18).over(175));
		assertEquals(QQ.of(33).over(35), r.getCoefficient());
		assertEquals(14L, r.getRadicand());
	}

}
