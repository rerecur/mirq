package xyz.mirq.radicals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SurdAdditiveTest {

	static Surd a;
	static Surd b;
	static Surd c;

	@BeforeAll
	static void beforeAll() {
		a = Surd.of(5, 3);
		b = Surd.of(5, 2);
		c = Surd.of(1, 3);
	}

	@Test
	void testPlusEmpty() {
		final Optional<Surd> sum = a.plus(b);
		assertFalse(sum.isPresent());
	}

	@Test
	void plusZeroShouldBeNeutral() {
		final Optional<Surd> sum = a.plus(Surd.ZERO);
		assertTrue(sum.isPresent());
		assertEquals(a, sum.get());
	}

	@Test
	void zeroPlusShouldBeNeutral() {
		final Optional<Surd> sum = a.sumByAugend(Surd.ZERO);
		assertTrue(sum.isPresent());
		assertEquals(a, sum.get());
	}

	@Test
	void testPlusPresent() {
		final Optional<Surd> sum = a.plus(c);
		assertTrue(sum.isPresent());
		assertEquals(Surd.of(6, 3), sum.get());
	}

	@Test
	void testSumByAugend() {
		final Optional<Surd> sum = a.sumByAugend(c);
		assertTrue(sum.isPresent());
		assertEquals(Surd.of(6, 3), sum.get());
	}

	@Test
	void testGetZero() {
		assertEquals(Surd.ZERO, a.getZero());
	}

	@Test
	void testIsZero() {
		assertTrue(a.getZero().isZero());
	}

	@Test
	void testOpposite() {
		final Surd negation = a.opposite();
		assertEquals(Surd.of(-5, 3), negation);
	}

	@Test
	void testMinusEmpty() {
		final Optional<Surd> difference = a.minus(b);
		assertFalse(difference.isPresent());
	}

	@Test
	void testMinusPresent() {
		final Optional<Surd> difference = a.minus(c);
		assertTrue(difference.isPresent());
		assertEquals(Surd.of(4, 3), difference.get());
	}
}
