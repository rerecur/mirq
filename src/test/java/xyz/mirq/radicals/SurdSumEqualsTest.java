package xyz.mirq.radicals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SurdSumEqualsTest {

	static SurdSum example;
	static SurdSum copy;

	@BeforeAll
	static void beforeAll() {
		example = SurdSum.of(
			Surd.of(1, 3),
			Surd.of( 10, 2),
			Surd.UNIT,
			Surd.of(17)
		);

		/* Our copy is the same value, but constructed differently */
		copy = SurdSum.of(
			Surd.of(18),
			Surd.of( 10, 2),
			Surd.of(1, 3)
		);

	}

	@Test
	@SuppressWarnings("squid:S5785") // Deliberately testing == operator
	void testSelf() {
		final SurdSum self = example.self();
		assertTrue(example == self);
	}

	@Test
	void testDifferentHashCodes(){
		assertNotEquals(SurdSum.UNIT.hashCode(), SurdSum.ZERO.hashCode());
		assertNotEquals(example.hashCode(), SurdSum.UNIT.hashCode());
		assertNotEquals(example.hashCode(), SurdSum.ZERO.hashCode());
	}

	@Test
	void testSameHashCodes(){
		assertEquals(example.hashCode(), copy.hashCode());
		assertEquals(SurdSum.UNIT.hashCode(), SurdSum.UNIT.hashCode());
		assertEquals(SurdSum.ZERO.hashCode(), SurdSum.ZERO.hashCode());
	}

	@Test
	@SuppressWarnings("squid:S5785") // Deliberately testing equals
	void testEqualsSameObject() {
		assertTrue(example.equals(example));
	}

	@Test
	@SuppressWarnings("squid:S5785") // Deliberately testing equals
	void testEqualsDifferentTypes() {
		assertFalse(SurdSum.UNIT.equals("1"));
	}

	@Test
	@SuppressWarnings("squid:S5785") // Deliberately testing equals
	void testEqualsTrue() {
		assertTrue(example.equals(copy));
	}

	@Test
	@SuppressWarnings("squid:S5785") // Deliberately testing equals
	void testEqualsFalse() {
		assertFalse(example.equals(SurdSum.of(18)));
		assertFalse(SurdSum.UNIT.equals(SurdSum.ZERO));
	}

	@Test
	void testEqvTrue() {
		assertTrue(example.eqv(copy));
	}

	@Test
	void testEqvFalse() {
		assertFalse(example.eqv(SurdSum.of(18)));
		assertFalse(SurdSum.UNIT.eqv(SurdSum.ZERO));
	}

}
