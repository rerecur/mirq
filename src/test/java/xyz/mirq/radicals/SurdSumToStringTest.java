package xyz.mirq.radicals;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SurdSumToStringTest {

	@Test
	void zeroShouldBe0() {
		assertEquals("0", SurdSum.ZERO.toString());
	}

	@Test
	void unitShouldBe1() {
		assertEquals("1", SurdSum.UNIT.toString());
	}

	/*
	 * Ensure that a perfectly formed expression can be parsed
	 * and then converted back to the same string. Summands
	 * must be in increasing order of radicand, and so forth.
	 */
	void parseAndToString(String expression) {
		final SurdSum sum = SurdSum.parse(expression);
		assertEquals(expression, sum.toString());
	}

	@Test
	void zeroShouldParse() {
		parseAndToString("0");
	}

	@Test
	void rationalShouldParse() {
		parseAndToString("(2//3)");
	}

	@Test
	void complicatedSumShouldParse() {
		parseAndToString("(-4//3) + 6√2 + (3//7)√5 + (-10)√11");
	}

}
