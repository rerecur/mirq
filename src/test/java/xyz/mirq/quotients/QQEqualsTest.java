package xyz.mirq.quotients;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class QQEqualsTest {

	static QQ half;
	static QQ third;

	@BeforeAll
	static void beforeAll() {
		half = QQ.UNIT.over(2);
		third = QQ.UNIT.over(3);
	}

	@Test
	void testHashCode() {
		assertNotEquals(half.hashCode(), third.hashCode());
	}

	@Test
	@SuppressWarnings("squid:S5785") // Deliberately testing equals
	void testEqualsIdentically() {
		assertTrue(half.equals(half));
	}

	@Test
	@SuppressWarnings("squid:S5785") // Deliberately testing equals
	void testEqualsWrongType() {
		assertFalse(half.equals("1//2"));
	}

	@Test
	void testEqvDifferentNumerators() {
		assertFalse(QQ.UNIT.eqv(QQ.ZERO));
	}

	@Test
	void testEqvDifferentDenominators() {
		assertFalse(half.eqv(third));
	}

}
