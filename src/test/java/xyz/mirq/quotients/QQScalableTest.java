package xyz.mirq.quotients;

import org.junit.jupiter.api.Test;
import xyz.mirq.structures.Scalable;

import static org.junit.jupiter.api.Assertions.assertEquals;

class QQScalableTest {

	public static class Bar
		implements Scalable<QQ, Bar> {

		QQ q;

		@Override
		public Bar scaleBy(QQ scalar) {
			Bar product = new Bar();
			product.q = scalar.times(this.q);
			return product;
		}

	}

	@Test
	void testScaleBy() {
		final QQ three = QQ.of(3);

		final Bar bar = new Bar();
		bar.q = QQ.of(5);

		final Bar productRightToLeft = bar.scaleBy(three);
		assertEquals("15", productRightToLeft.q.toString());

		final Bar productLeftToRight = three.scale(bar);
		assertEquals("15", productLeftToRight.q.toString());
	}
}
