package xyz.mirq.quotients;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class QQConstructorTest {

	@Test
	void testOf() {
		assertTrue(QQ.ZERO.eqv(QQ.of(0)));
		assertTrue(QQ.UNIT.eqv(QQ.of(1)));

		final QQ negativeTwoThirds = QQ.of(12).over(-18);
		assertEquals(-2L, negativeTwoThirds.getNumerator());
		assertEquals(3L, negativeTwoThirds.getDenominator());
	}

	@Test
	void testZeroOverZero() {
		Assertions.assertThrows(ArithmeticException.class, () -> {
			QQ.ZERO.over(0);
		});
	}

	@Test
	void testUnitOverZero() {
		Assertions.assertThrows(ArithmeticException.class, () -> {
			QQ.UNIT.over(0);
		});
	}
}
