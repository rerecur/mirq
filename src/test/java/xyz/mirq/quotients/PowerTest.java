package xyz.mirq.quotients;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import xyz.mirq.core.Variable;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PowerTest {

	static Variable x;
	static Power x2;
	static Power x0;
	static Power y;
	static Power anotherY;
	static Power y0;
	static Power yInverse;

	@BeforeAll
	static void beforeAll() {
		x = Variable.of("x");

		x2 = Power.of(x, 2);
		x0 = Power.of(x, 0);

		y = Power.of(Variable.of("y"));
		anotherY = Power.of(Variable.of("y"));
		y0 = Power.of(Variable.of("y"), 0);
		yInverse = Power.of(Variable.of("y"), -1);
	}

	@Test
	void testConstructorWithDegree() {
		final Power power = Power.of(x, -2);
		assertEquals(x, power.getVariable());
		assertEquals(-2, power.getDegree());
	}

	@Test
	void testConstructorWithoutDegree() {
		final Power power = Power.of(x);
		assertEquals(x, power.getVariable());
		assertEquals(1, power.getDegree());
	}

	@Test
	void testToStringDegreeZero() {
		assertEquals("y⁰", y0.toString());
	}

	@Test
	void testToStringDegreeOne() {
		assertEquals("y", y.toString());
	}

	@Test
	void testToStringDegreeTwo() {
		assertEquals("x²", x2.toString());
	}

	@Test
	void testSelf() {
		assertTrue(y.self() instanceof Power);
	}

	@Test
	void testHashCode() {
		assertEquals(y.hashCode(), anotherY.hashCode());
		assertNotEquals(y.hashCode(), Power.of(x).hashCode());
		assertNotEquals(y.hashCode(), x2.hashCode());
	}

	@Test
	void testHashCodeDegreeZero() {
		assertEquals(x0.hashCode(), y0.hashCode());
	}

	@Test
	@SuppressWarnings("squid:S5785") // Deliberately testing equals
	void testEquals() {
		assertTrue(y.equals(y));
		assertFalse(y.equals("y"));
		assertTrue(y.equals(anotherY));
		assertFalse(y.equals(x2));
	}

	@Test
	void testEqv() {
		assertTrue(y.eqv(anotherY));
		assertFalse(y.eqv(y0));
		assertTrue(x0.eqv(y0));
		assertFalse(y.eqv(Power.of(x)));
	}

	@Test
	void testAccessors() {
		assertEquals(x2.degree, x2.getDegree());
		assertEquals(x2.variable, x2.getVariable());
	}

	@Test
	void testOneTimesX2() {
		final Optional<Power> product = y0.times(x2);
		assertTrue(product.isPresent());
		assertTrue(product.get().eqv(x2));
	}

	@Test
	void testX2TimesOne() {
		final Optional<Power> product = x2.times(y0);
		assertTrue(product.isPresent());
		assertTrue(product.get().eqv(x2));
	}

	@Test
	void testX2TimesY() {
		final Optional<Power> product = x2.times(y);
		assertFalse(product.isPresent());
	}

	@Test
	void testX2TimesX() {
		final Optional<Power> product = x2.times(Power.of(x));
		assertTrue(product.isPresent());
		assertEquals(x, product.get().getVariable());
		assertEquals(3, product.get().getDegree());
	}

	@Test
	void testGetUnit() {
		assertEquals(0, y.getUnit().getDegree());
		assertEquals("y", y.getVariable().toString());

		assertEquals(0, y0.getDegree());
		assertEquals("y", y0.getVariable().toString());
	}

	@Test
	void testInverseOfUnit() {
		final Optional<Power> power = y0.inverse();
		assertTrue(power.isPresent());
		assertEquals("y", power.get().getVariable().toString());
		assertEquals(0, power.get().getDegree());
	}

	@Test
	void testInverse() {
		final Optional<Power> power = x2.inverse();
		assertTrue(power.isPresent());
		assertEquals("x", power.get().getVariable().toString());
		assertEquals(-2, power.get().getDegree());
	}

}
