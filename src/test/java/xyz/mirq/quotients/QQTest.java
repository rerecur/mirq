package xyz.mirq.quotients;

import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class QQTest {

	@Test
	void testOperators() {
		final QQ five = QQ.of(5);

		assertEquals("6", QQ.UNIT.plus(five).toString());
		assertEquals("(-4)", QQ.UNIT.minus(five).toString());
		assertEquals("5", QQ.UNIT.times(five).toString());
		assertEquals("(1//5)", QQ.UNIT.over(five).toString());
	}

	@Test
	void testConvenienceMethods() {
		assertEquals("6", QQ.UNIT.plus(5).toString());
		assertEquals("(-4)", QQ.UNIT.minus(5).toString());
		assertEquals("5", QQ.UNIT.times(5).toString());
		assertEquals("(1//5)", QQ.UNIT.over(5).toString());
	}

	@Test
	void testRelations() {
		assertTrue(QQ.ZERO.lt(QQ.UNIT));
		assertTrue(QQ.ZERO.lte(QQ.UNIT));
		assertFalse(QQ.ZERO.gt(QQ.UNIT));
		assertFalse(QQ.ZERO.gte(QQ.UNIT));

		assertFalse(QQ.UNIT.lt(QQ.UNIT));
		assertTrue(QQ.UNIT.lte(QQ.UNIT));
		assertFalse(QQ.UNIT.gt(QQ.UNIT));
		assertTrue(QQ.UNIT.gte(QQ.UNIT));
	}

	@Test
	void testCompareTo() {
		assertEquals(0, QQ.UNIT.compareTo(QQ.UNIT));
		assertTrue(QQ.UNIT.compareTo(QQ.ZERO) > 0);
		assertTrue(QQ.ZERO.compareTo(QQ.UNIT) < 0);
	}

	@Test
	void testGetUnit() {
		final QQ q = QQ.ZERO.getUnit();
		assertEquals(1, q.getNumerator());
		assertEquals(1, q.getDenominator());
	}

	@Test
	void testInverseEmpty() {
		final Optional<QQ> q = QQ.UNIT.undo(QQ.ZERO);
		assertFalse(q.isPresent());
	}
}
