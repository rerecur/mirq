package xyz.mirq.quotients;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class QQToStringTest {

	@Test
	void testZero() {
		final String s = "0";

		assertEquals(s, QQ.ZERO.toString());
		assertTrue(QQ.ZERO.eqv(QQ.parse(s)));
	}

	@Test
	void testPositiveInteger() {
		final QQ q = QQ.of(5);
		final String s = "5";

		assertEquals(s, q.toString());
		assertTrue(q.eqv(QQ.parse(s)));
	}

	@Test
	void testNegativeInteger() {
		final QQ q = QQ.of(-5);
		final String s = "(-5)";

		assertEquals(s, q.toString());
		assertTrue(q.eqv(QQ.parse(s)));
	}

	@Test
	void testPositiveFraction() {
		final QQ q = QQ.of(2).over(3);
		final String s = "(2//3)";

		assertEquals(s, q.toString());
		assertTrue(q.eqv(QQ.parse(s)));
	}

	@Test
	void testnegativeFraction() {
		final QQ q = QQ.of(2).over(-3);
		final String s = "(-2//3)";

		assertEquals(s, q.toString());
		assertTrue(q.eqv(QQ.parse(s)));
	}

	@Test
	void testReduction() {
		final QQ q = QQ.of(21).over(6);
		final String s = "(7//2)";

		assertEquals(s, q.toString());
		assertTrue(q.eqv(QQ.parse(s)));
	}

	@Test
	void testWhitespace() {
		Assertions.assertThrows(NumberFormatException.class, () -> {
			QQ.parse(" ");
		});
	}

	@Test
	void testNegativeDenominator() {
		Assertions.assertThrows(NumberFormatException.class, () -> {
			QQ.parse("2//-3");
		});
	}

	@Test
	void testMissingNumerator() {
		Assertions.assertThrows(NumberFormatException.class, () -> {
			QQ.parse("//3");
		});
	}

	@Test
	void testZeroDenominator() {
		Assertions.assertThrows(NumberFormatException.class, () -> {
			QQ.parse("2//0");
		});
	}

}
