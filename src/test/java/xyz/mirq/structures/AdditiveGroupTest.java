package xyz.mirq.structures;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AdditiveGroupTest {

	public static class Foo
		implements AdditiveGroup<Foo> {

		public int n;

		@Override
		public Foo opposite() {
			final Foo foo = new Foo();
			foo.n = -this.n;
			return foo;
		}

		@Override
		public Foo getZero() {
			final Foo zero = new Foo();
			zero.n = 0;
			return zero;
		}

		@Override
		public Foo plus(Foo that) {
			final Foo sum = new Foo();
			sum.n = this.n + that.n;
			return sum;
		}

		@Override
		public boolean eqv(Foo that) {
			return this == that || this.n == that.n;
		}

		@Override
		public Foo self() {
			return this;
		}
	}

	static Foo a;

	@BeforeAll
	static void beforeAll() {
		a = new Foo();
		a.n = 3;
	}

	@Test
	void testOpposite() {
		final Foo foo = a.opposite();
		assertEquals(-a.n, foo.n);
	}

	@Test
	void testMinus() {
		assertTrue(a.minus(a).isZero());
	}

}
