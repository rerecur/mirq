package xyz.mirq.structures;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MultiplicativeGroupTest {

	public static class Foo
		implements MultiplicativeGroup<Foo> {

		/*
		 * Not truly a real number, but good enough for testing,
		 * especially if we keep it a power of two.
		 */
		public double x;

		@Override
		public Optional<Foo> inverse() {
			if (this.x == 0.0) {
				return Optional.empty();
			}

			final Foo foo = new Foo();
			foo.x = 1.0 / this.x;
			return Optional.of(foo);
		}

		@Override
		public Foo getUnit() {
			final Foo unit = new Foo();
			unit.x = 1.0;
			return unit;
		}

		@Override
		public Foo times(Foo multiplicand) {
			final Foo product = new Foo();
			product.x = this.x * multiplicand.x;
			return product;
		}

		@Override
		public boolean eqv(Foo that) {
			return this == that || this.x == that.x;
		}

		@Override
		public Foo self() {
			return this;
		}
	}

	static Foo eight;
	static Foo zero;

	@BeforeAll
	static void beforeAll() {
		eight = new Foo();
		eight.x = 8.0;

		zero = new Foo();
		zero.x = 0.0;
	}

	@Test
	void testInverseOfZero() {
		final Optional<Foo> foo = zero.inverse();
		assertFalse(foo.isPresent());
	}

	@Test
	void testInverseOfNonZero() {
		final Optional<Foo> foo = eight.inverse();
		assertTrue(foo.isPresent());
		assertEquals(0.125, foo.get().x);
	}

	@Test
	void testReciprocalOfZero() {
		Assertions.assertThrows(ArithmeticException.class, () -> {
			zero.reciprocal();
		});
	}

	@Test
	void testReciprocalOfNonZero() {
		final Foo foo = eight.reciprocal();
		assertEquals(0.125, foo.x);
	}

	@Test
	void testOver() {
		assertTrue(eight.over(eight).isUnit());
	}

	@Test
	void testOverThrows() {
		Assertions.assertThrows(ArithmeticException.class, () -> {
			eight.over(zero);
		});
	}

	@Test
	void testUndoEmpty() {
		final Optional<Foo> foo = eight.undo(zero);
		assertFalse(foo.isPresent());
	}

	@Test
	void testUndo() {
		final Optional<Foo> foo = eight.undo(eight);
		assertTrue(foo.isPresent());
		assertTrue(foo.get().isUnit());
	}

}
