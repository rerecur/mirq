package xyz.mirq.structures;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SignedTest {

	public static class Foo
		implements Signed<Foo> {

		public int n;

		@Override
		public Foo self() {
			return this;
		}

		@Override
		public boolean eqv(Foo that) {
			return this.n == that.n;
		}

		@Override
		public Foo getZero() {
			final Foo zero = new Foo();
			zero.n = 0;
			return zero;
		}

		@Override
		public boolean isPositive() {
			return n > 0;
		}

		@Override
		public boolean isNegative() {
			return n < 0;
		}

		@Override
		public int signum() {
			if (isZero()) {
				return 0;
			} else if (isPositive()) {
				return +1;
			} else {
				return -1;
			}
		}

		@Override
		public Foo abs() {
			if (isNegative()) {
				final Foo foo = new Foo();
				foo.n = -n;
				return foo;
			}
			return this;
		}
	}

	static Foo four;
	static Foo zero;
	static Foo negativeTwo;

	@BeforeAll
	static void beforeAll() {
		four = new Foo();
		four.n = 4;

		zero = four.getZero();

		negativeTwo = new Foo();
		negativeTwo.n = -2;
	}

	@Test
	void testIsPositive() {
		assertTrue(four.isPositive());
		assertFalse(zero.isPositive());
		assertFalse(negativeTwo.isPositive());
	}

	@Test
	void testIsNegative() {
		assertFalse(four.isNegative());
		assertFalse(zero.isNegative());
		assertTrue(negativeTwo.isNegative());
	}

	@Test
	void testSignum() {
		assertEquals(1, four.signum());
		assertEquals(0, zero.signum());
		assertEquals(-1, negativeTwo.signum());
	}

	@Test
	void testAbs() {
		assertEquals(4, four.abs().n);
		assertEquals(0, zero.abs().n);
		assertEquals(2, negativeTwo.abs().n);
	}

}
