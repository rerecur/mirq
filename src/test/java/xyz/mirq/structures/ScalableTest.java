package xyz.mirq.structures;

import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ScalableTest {

	public static class Foo
		implements Field<Foo> {

		public double x;  // Floating points are not truly a field

		@Override
		public Foo self() {
			return this;
		}

		@Override
		public boolean eqv(Foo that) {
			return this == that || this.x == that.x;
		}

		@Override
		public Foo plus(Foo that) {
			final Foo sum = new Foo();
			sum.x = this.x + that.x;
			return sum;
		}

		@Override
		public Foo getZero() {
			final Foo zero = new Foo();
			zero.x = 0.0;
			return zero;
		}

		@Override
		public Foo opposite() {
			final Foo foo = new Foo();
			foo.x = -this.x;
			return foo;
		}

		@Override
		public Foo times(Foo multiplicand) {
			final Foo product = new Foo();
			product.x = this.x * multiplicand.x;
			return product;
		}

		@Override
		public Foo getUnit() {
			final Foo unit = new Foo();
			unit.x = 1.0;
			return unit;
		}

		@Override
		public Optional<Foo> inverse() {
			if (x == 0.0) {
				return Optional.empty();
			}

			final Foo foo = new Foo();
			foo.x = 1.0 / this.x;
			return Optional.of(foo);
		}
	}

	public static class Bar
		implements Scalable<Foo, Bar> {

		Foo y;

		@Override
		public Bar scaleBy(Foo scalar) {
			Bar product = new Bar();
			product.y = scalar.times(this.y);
			return product;
		}

	}

	@Test
	void testScaleBy() {
		final Foo foo = new Foo();
		foo.x = 3.0;

		final Bar bar = new Bar();
		bar.y = new Foo();
		bar.y.x = 5.0;

		final Bar productRightToLeft = bar.scaleBy(foo);
		assertEquals(15.0, productRightToLeft.y.x);

		final Bar productLeftToRight = foo.scale(bar);
		assertEquals(15.0, productLeftToRight.y.x);
	}

}
