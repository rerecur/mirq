package xyz.mirq.structures;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AdditiveGroupoidTest {

	public static class Foo
		implements AdditiveGroupoid<Foo> {

		public int n;

		@Override
		public Foo self() {
			return this;
		}

		@Override
		public boolean eqv(Foo that) {
			return this == that || this.n == that.n;
		}

		@Override
		public Optional<Foo> plus(Foo that) {
			final int sum = this.n + that.n;
			/* Only even sums are allowed in this contrived example */
			if (0 == sum % 2) {
				final Foo foo = new Foo();
				foo.n = sum;
				return Optional.of(foo);
			} else {
				return Optional.empty();
			}
		}

		@Override
		public Foo getZero() {
			final Foo zero = new Foo();
			zero.n = 0;
			return zero;
		}

		@Override
		public Foo opposite() {
			final Foo foo = new Foo();
			foo.n = -n;
			return foo;
		}
	}

	static Foo a;
	static Foo b;
	static Foo c;

	@BeforeAll
	static void beforeAll() {
		a = new Foo();
		a.n = 1;

		b = new Foo();
		b.n = 2;

		c = new Foo();
		c.n = 3;
	}

	@Test
	void testOpposite() {
		final Foo negation = a.opposite();
		assertEquals(-a.n, negation.n);
	}

	@Test
	void testMinusEmpty() {
		final Optional<Foo> difference = a.minus(b);
		assertFalse(difference.isPresent());
	}

	@Test
	void testMinusPresent() {
		final Optional<Foo> difference = a.minus(c);
		assertTrue(difference.isPresent());
		assertEquals(-2, difference.get().n);
	}
}
