package xyz.mirq.structures;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class RingTest {

	public static class Foo
		implements Ring<Foo> {

		public int n;

		@Override
		public Foo opposite() {
			final Foo foo = new Foo();
			foo.n = -this.n;
			return foo;
		}

		@Override
		public Foo getZero() {
			final Foo zero = new Foo();
			zero.n = 0;
			return zero;
		}

		@Override
		public Foo plus(Foo that) {
			final Foo sum = new Foo();
			sum.n = this.n + that.n;
			return sum;
		}

		@Override
		public Foo getUnit() {
			final Foo unit = new Foo();
			unit.n = 1;
			return unit;
		}

		@Override
		public Foo times(Foo multiplicand) {
			final Foo product = new Foo();
			product.n = this.n * multiplicand.n;
			return product;
		}

		@Override
		public boolean eqv(Foo that) {
			return this == that || this.n == that.n;
		}

		@Override
		public Foo self() {
			return this;
		}
	}

	static Foo a;
	static Foo b;

	@BeforeAll
	static void beforeAll() {
		a = new Foo();
		a.n = 3;

		b = new Foo();
		b.n = 5;
	}

	@Test
	void testCommutator() {
		assertTrue(a.commutator(a).isZero());
		assertTrue(a.commutator(b).isZero());
	}

}
