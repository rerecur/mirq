package xyz.mirq.structures;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import xyz.mirq.typeclasses.Self;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class InvertibleTest {

	public static class Foo
		implements Self<Foo>, Invertible<Foo> {

		/*
		 * Not truly a real number, but okay enough for testing,
		 * especially if we keep it a power of two.
		 */
		public double x;

		@Override
		public Foo self() {
			return this;
		}

		@Override
		public Optional<Foo> inverse() {
			if (x == 0.0) {
				return Optional.empty();
			}

			final Foo foo = new Foo();
			foo.x = 1.0 / x;
			return Optional.of(foo);
		}

	}

	static Foo four;
	static Foo zero;

	@BeforeAll
	static void beforeAll() {
		four = new Foo();
		four.x = 4.0;

		zero = new Foo();
		zero.x = 0.0;
	}

	@Test
	void testInverseEmpty() {
		final Optional<Foo> foo = zero.inverse();
		assertFalse(foo.isPresent());
	}

	@Test
	void testInverseNotEmpty() {
		final Optional<Foo> foo = four.inverse();
		assertTrue(foo.isPresent());
		assertEquals(0.25, foo.get().x);
	}

	@Test
	void testReciprocalZero() {
		Assertions.assertThrows(ArithmeticException.class, () -> {
			zero.reciprocal();
		});
	}

	@Test
	void testReciprocalNotZero() {
		final Foo foo = four.reciprocal();
		assertEquals(0.25, foo.x);
	}

}
