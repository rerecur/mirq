package xyz.mirq.structures;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AdditiveSmallCategoryTest {

	public static class Foo
		implements AdditiveSmallCategory<Foo> {

		public int n;

		@Override
		public Foo self() {
			return this;
		}

		@Override
		public boolean eqv(Foo that) {
			return this == that || this.n == that.n;
		}

		@Override
		public Optional<Foo> plus(Foo that) {
			final int sum = this.n + that.n;
			/* Only even sums are allowed in this contrived example */
			if (0 == sum % 2) {
				final Foo foo = new Foo();
				foo.n = sum;
				return Optional.of(foo);
			} else {
				return Optional.empty();
			}
		}

		@Override
		public Foo getZero() {
			final Foo zero = new Foo();
			zero.n = 0;
			return zero;
		}
	}

	static Foo zero;
	static Foo five;

	@BeforeAll
	static void beforeAll() {
		zero = new Foo();
		zero.n = 0;

		five = new Foo();
		five.n = 5;
	}

	@Test
	void testGetZero() {
		Foo foo = five.getZero();
		assertEquals(0, foo.n);
	}

	@Test
	void testIsZero() {
		assertTrue(zero.isZero());
		assertFalse(five.isZero());
	}

}
