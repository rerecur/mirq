package xyz.mirq.structures;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class HasUnitTest {

	public static class Foo
		implements HasUnit<Foo> {

		public int n;

		@Override
		public Foo self() {
			return this;
		}

		@Override
		public boolean eqv(Foo that) {
			return this == that || this.n == that.n;
		}

		@Override
		public Foo getUnit() {
			final Foo unit = new Foo();
			unit.n = 1;
			return unit;
		}

	}

	static Foo five;
	static Foo unit;

	@BeforeAll
	static void beforeAll() {
		five = new Foo();
		five.n = 5;

		unit = five.getUnit();
	}

	@Test
	void testGetUnit() {
		assertEquals(1, unit.n);
	}

	@Test
	void testIsUnit() {
		assertFalse(five.isUnit());
		assertTrue(unit.isUnit());
	}

}
