package xyz.mirq.structures;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AdditiveSemiGroupTest {

	public static class Foo
		implements AdditiveSemiGroup<Foo> {

		public int n;

		@Override
		public Foo plus(Foo that) {
			final Foo sum = new Foo();
			sum.n = this.n + that.n;
			return sum;
		}

		@Override
		public boolean eqv(Foo that) {
			return this == that || this.n == that.n;
		}

		@Override
		public Foo self() {
			return this;
		}
	}

	static Foo a;
	static Foo b;

	@BeforeAll
	static void beforeAll() {
		a = new Foo();
		a.n = 3;

		b = new Foo();
		b.n = 5;
	}

	@Test
	void testPlus() {
		final Foo sum = a.plus(b);
		assertEquals(8, sum.n);
	}

	@Test
	void testSumByAugend() {
		final Foo sum = a.sumByAugend(b);
		assertEquals(8, sum.n);
	}

}
