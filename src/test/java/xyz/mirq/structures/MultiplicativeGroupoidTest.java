package xyz.mirq.structures;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MultiplicativeGroupoidTest {

	public static class Foo
		implements MultiplicativeGroupoid<Foo> {

		public double x;

		@Override
		public Foo self() {
			return this;
		}

		@Override
		public boolean eqv(Foo that) {
			return this == that || this.x == that.x;
		}

		@Override
		public Optional<Foo> times(Foo that) {
			final double product = this.x * that.x;
			/* Only positives are allowed in this contrived example */
			if (0.0 < product) {
				final Foo foo = new Foo();
				foo.x = product;
				return Optional.of(foo);
			} else {
				return Optional.empty();
			}
		}

		@Override
		public Foo getUnit() {
			final Foo unit = new Foo();
			unit.x = 1.0;
			return unit;
		}

		@Override
		public Optional<Foo> inverse() {
			/* Only positives have inverses in this contrived example */
			if (0.0 < x) {
				final Foo foo = new Foo();
				foo.x = 1.0 / x;
				return Optional.of(foo);
			} else {
				return Optional.empty();
			}
		}

	}

	static Foo four;
	static Foo negativeTwo;

	@BeforeAll
	static void beforeAll() {
		four = new Foo();
		four.x = 4.0;

		negativeTwo = new Foo();
		negativeTwo.x = -2.0;
	}

	@Test
	void testInverseEmpty() {
		final Optional<Foo> foo = negativeTwo.inverse();
		assertFalse(foo.isPresent());
	}

	@Test
	void testInversePresent() {
		final Optional<Foo> foo = four.inverse();
		assertTrue(foo.isPresent());
		assertEquals(0.25, foo.get().x);
	}

	@Test
	void testReciprocalThrow() {
		Assertions.assertThrows(ArithmeticException.class, () -> {
			negativeTwo.reciprocal();
		});
	}

	@Test
	void testReciprocalNoThrow() {
		final Foo foo = four.reciprocal();
		assertEquals(0.25, foo.x);
	}

	@Test
	void testOverEmpty() {
		final Optional<Foo> foo = four.over(negativeTwo);
		assertFalse(foo.isPresent());
	}

	@Test
	void testOverPresent() {
		final Optional<Foo> foo = four.over(four);
		assertTrue(foo.isPresent());
		assertEquals(1.0, foo.get().x);
	}

	@Test
	void testUndoEmpty() {
		final Optional<Foo> foo = four.undo(negativeTwo);
		assertFalse(foo.isPresent());
	}

	@Test
	void testUndoPresent() {
		final Optional<Foo> foo = four.over(four);
		assertTrue(foo.isPresent());
		assertEquals(1.0, foo.get().x);
	}

}
