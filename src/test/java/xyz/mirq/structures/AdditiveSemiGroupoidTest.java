package xyz.mirq.structures;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AdditiveSemiGroupoidTest {

	public static class Foo
		implements AdditiveSemiGroupoid<Foo> {

		public int n;

		@Override
		public Foo self() {
			return this;
		}

		@Override
		public boolean eqv(Foo that) {
			return this == that || this.n == that.n;
		}

		@Override
		public Optional<Foo> plus(Foo that) {
			final int sum = this.n + that.n;
			/* Only even sums are allowed in this contrived example */
			if (0 == sum % 2) {
				final Foo foo = new Foo();
				foo.n = sum;
				return Optional.of(foo);
			} else {
				return Optional.empty();
			}
		}
	}

	static Foo a;
	static Foo b;
	static Foo c;

	@BeforeAll
	static void beforeAll() {
		a = new Foo();
		a.n = 1;

		b = new Foo();
		b.n = 2;

		c = new Foo();
		c.n = 3;
	}

	@Test
	void testPlusEmpty() {
		final Optional<Foo> sum = a.plus(b);
		assertFalse(sum.isPresent());
	}

	@Test
	void testPlusPresent() {
		final Optional<Foo> sum = a.plus(c);
		assertTrue(sum.isPresent());
		assertEquals(4, sum.get().n);
	}

	@Test
	void testSumByAugend() {
		final Optional<Foo> sum = a.sumByAugend(c);
		assertTrue(sum.isPresent());
		assertEquals(4, sum.get().n);
	}

}
