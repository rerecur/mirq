package xyz.mirq.structures;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class HasZeroTest {

	public static class Foo
		implements HasZero<Foo> {

		public int n;

		@Override
		public Foo self() {
			return this;
		}

		@Override
		public boolean eqv(Foo that) {
			return this == that || this.n == that.n;
		}

		@Override
		public Foo getZero() {
			final Foo zero = new Foo();
			zero.n = 0;
			return zero;
		}

	}

	static Foo five;
	static Foo zero;

	@BeforeAll
	static void beforeAll() {
		five = new Foo();
		five.n = 5;

		zero = five.getZero();
	}

	@Test
	void testGetZero() {
		assertEquals(0, zero.n);
	}

	@Test
	void testIsZero() {
		assertFalse(five.isZero());
		assertTrue(zero.isZero());
	}

}
