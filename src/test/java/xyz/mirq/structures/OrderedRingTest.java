package xyz.mirq.structures;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class OrderedRingTest {

	public static class Foo
		implements OrderedRing<Foo> {

		public int n;

		@Override
		public Foo self() {
			return this;
		}

		@Override
		public boolean eqv(Foo that) {
			return this.n == that.n;
		}

		@Override
		public Foo plus(Foo that) {
			final Foo foo = new Foo();
			foo.n = this.n + that.n;
			return foo;
		}

		@Override
		public Foo getZero() {
			final Foo foo = new Foo();
			foo.n = 0;
			return foo;
		}

		@Override
		public Foo opposite() {
			final Foo foo = new Foo();
			foo.n = -n;
			return foo;
		}

		@Override
		public Foo times(Foo that) {
			final Foo foo = new Foo();
			foo.n = this.n * that.n;
			return foo;
		}

		@Override
		public Foo getUnit() {
			final Foo foo = new Foo();
			foo.n = 1;
			return foo;
		}

		@Override
		public boolean lt(Foo that) {
			return this.n < that.n;
		}
	}

	static Foo four;
	static Foo zero;
	static Foo negativeTwo;

	@BeforeAll
	static void beforeAll() {
		four = new Foo();
		four.n = 4;

		zero = four.getZero();

		negativeTwo = new Foo();
		negativeTwo.n = -2;
	}

	@Test
	void testIsPositive() {
		assertTrue(four.isPositive());
		assertFalse(zero.isPositive());
		assertFalse(negativeTwo.isPositive());
	}

	@Test
	void testIsNegative() {
		assertFalse(four.isNegative());
		assertFalse(zero.isNegative());
		assertTrue(negativeTwo.isNegative());
	}

	@Test
	void testSignum() {
		assertEquals(1, four.signum());
		assertEquals(0, zero.signum());
		assertEquals(-1, negativeTwo.signum());
	}

	@Test
	void testAbs() {
		assertEquals(4, four.abs().n);
		assertEquals(0, zero.abs().n);
		assertEquals(2, negativeTwo.abs().n);
	}

}
