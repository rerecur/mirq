package xyz.mirq.structures;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MultiplicativeSemiGroupTest {

	public static class Foo
		implements MultiplicativeSemiGroup<Foo> {

		public int n;

		@Override
		public Foo times(Foo multiplicand) {
			final Foo product = new Foo();
			product.n = this.n * multiplicand.n;
			return product;
		}

		@Override
		public boolean eqv(Foo that) {
			return this == that || this.n == that.n;
		}

		@Override
		public Foo self() {
			return this;
		}
	}

	static Foo a;
	static Foo b;

	@BeforeAll
	static void beforeAll() {
		a = new Foo();
		a.n = 3;

		b = new Foo();
		b.n = 5;
	}

	@Test
	void testTimes() {
		final Foo product = a.times(b);
		assertEquals(15, product.n);
	}

	@Test
	void testProductByMultiplier() {
		final Foo product = a.productByMultiplier(b);
		assertEquals(15, product.n);
	}

}
