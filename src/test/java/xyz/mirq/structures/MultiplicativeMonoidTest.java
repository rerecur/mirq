package xyz.mirq.structures;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MultiplicativeMonoidTest {

	public static class Foo
		implements MultiplicativeMonoid<Foo> {

		public int n;

		@Override
		public Foo getUnit() {
			final Foo unit = new Foo();
			unit.n = 1;
			return unit;
		}

		@Override
		public Foo times(Foo multiplicand) {
			final Foo product = new Foo();
			product.n = this.n * multiplicand.n;
			return product;
		}

		@Override
		public boolean eqv(Foo that) {
			return this == that || this.n == that.n;
		}

		@Override
		public Foo self() {
			return this;
		}
	}

	static Foo a;

	@BeforeAll
	static void beforeAll() {
		a = new Foo();
		a.n = 3;
	}

	@Test
	void testGetUnit() {
		final Foo unit = a.getUnit();
		assertTrue(unit.isUnit());
		assertEquals(1, unit.n);
	}

	@Test
	void testIsUnit() {
		assertFalse(a.isUnit());
	}

}
