package xyz.mirq.structures;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MultiplicativeSmallCategoryTest {

	public static class Foo
		implements MultiplicativeSmallCategory<Foo> {

		public int n;

		@Override
		public Foo self() {
			return this;
		}

		@Override
		public boolean eqv(Foo that) {
			return this == that || this.n == that.n;
		}

		@Override
		public Optional<Foo> times(Foo that) {
			final int product = this.n * that.n;
			/* Only positives are allowed in this contrived example */
			if (0 < product) {
				final Foo foo = new Foo();
				foo.n = product;
				return Optional.of(foo);
			} else {
				return Optional.empty();
			}
		}

		@Override
		public Foo getUnit() {
			final Foo unit = new Foo();
			unit.n = 1;
			return unit;
		}
	}

	static Foo five;
	static Foo unit;

	@BeforeAll
	static void beforeAll() {
		five = new Foo();
		five.n = 5;

		unit = five.getUnit();
	}

	@Test
	void testGetZero() {
		Foo foo = five.getUnit();
		assertEquals(1, foo.n);
	}

	@Test
	void testIsUnit() {
		assertTrue(unit.isUnit());
		assertFalse(five.isUnit());
	}

}
