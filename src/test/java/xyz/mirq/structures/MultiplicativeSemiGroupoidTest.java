package xyz.mirq.structures;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MultiplicativeSemiGroupoidTest {

	public static class Foo
		implements MultiplicativeSemiGroupoid<Foo> {

		public int n;

		@Override
		public Foo self() {
			return this;
		}

		@Override
		public boolean eqv(Foo that) {
			return this == that || this.n == that.n;
		}

		@Override
		public Optional<Foo> times(Foo that) {
			final int product = this.n * that.n;
			/* Only positives are allowed in this contrived example */
			if (0 < product) {
				final Foo foo = new Foo();
				foo.n = product;
				return Optional.of(foo);
			} else {
				return Optional.empty();
			}
		}
	}

	static Foo a;
	static Foo b;
	static Foo c;

	@BeforeAll
	static void beforeAll() {
		a = new Foo();
		a.n = 5;

		b = new Foo();
		b.n = -2;

		c = new Foo();
		c.n = 3;
	}

	@Test
	void testTimesEmpty() {
		final Optional<Foo> product = a.times(b);
		assertFalse(product.isPresent());
	}

	@Test
	void testTimesPresent() {
		final Optional<Foo> product = a.times(c);
		assertTrue(product.isPresent());
		assertEquals(15, product.get().n);
	}

	@Test
	void testProductByMultiplier() {
		final Optional<Foo> product = a.productByMultiplier(c);
		assertTrue(product.isPresent());
		assertEquals(15, product.get().n);
	}

}
