package xyz.mirq.structures;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FieldTest {

	public static class Foo
		implements Field<Foo> {

		public double x;  // Floating points are not truly a field

		@Override
		public Foo self() {
			return this;
		}

		@Override
		public boolean eqv(Foo that) {
			return this == that || this.x == that.x;
		}

		@Override
		public Foo plus(Foo that) {
			final Foo sum = new Foo();
			sum.x = this.x + that.x;
			return sum;
		}

		@Override
		public Foo getZero() {
			final Foo zero = new Foo();
			zero.x = 0.0;
			return zero;
		}

		@Override
		public Foo opposite() {
			final Foo foo = new Foo();
			foo.x = -this.x;
			return foo;
		}

		@Override
		public Foo times(Foo multiplicand) {
			final Foo product = new Foo();
			product.x = this.x * multiplicand.x;
			return product;
		}

		@Override
		public Foo getUnit() {
			final Foo unit = new Foo();
			unit.x = 1.0;
			return unit;
		}

		@Override
		public Optional<Foo> inverse() {
			if (x == 0.0) {
				return Optional.empty();
			}

			final Foo foo = new Foo();
			foo.x = 1.0 / this.x;
			return Optional.of(foo);
		}
	}

	static Foo a;
	static Foo b;

	@BeforeAll
	static void beforeAll() {
		a = new Foo();
		a.x = 3.0;

		b = new Foo();
		b.x = 2.0;
	}

	@Test
	void testOver() {
		final Foo quotient = a.over(b);
		assertEquals(1.5, quotient.x);
	}

	@Test
	void testUndo() {
		final Optional<Foo> quotient = a.undo(b);
		assertTrue(quotient.isPresent());
		assertEquals(1.5, quotient.get().x);
	}

}
