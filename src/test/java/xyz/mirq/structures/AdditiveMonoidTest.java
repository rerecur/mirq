package xyz.mirq.structures;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AdditiveMonoidTest {

	public static class Foo
		implements AdditiveMonoid<Foo> {

		public int n;

		@Override
		public Foo getZero() {
			final Foo zero = new Foo();
			zero.n = 0;
			return zero;
		}

		@Override
		public Foo plus(Foo that) {
			final Foo sum = new Foo();
			sum.n = this.n + that.n;
			return sum;
		}

		@Override
		public boolean eqv(Foo that) {
			return this == that || this.n == that.n;
		}

		@Override
		public Foo self() {
			return this;
		}
	}

	static Foo a;

	@BeforeAll
	static void beforeAll() {
		a = new Foo();
		a.n = 3;
	}

	@Test
	void testGetZero() {
		final Foo zero = a.getZero();
		assertTrue(zero.isZero());
		assertEquals(0, zero.n);
	}

	@Test
	void testIsZero() {
		assertFalse(a.isZero());
	}

}
