package xyz.mirq.typeclasses;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class OrderTest {

	public static class Foo
		implements Order<Foo> {

		public int n;

		@Override
		public boolean lt(Foo that) {
			return this.n < that.n;
		}

		@Override
		public Foo self() {
			return this;
		}
	}

	static Foo a;
	static Foo b;
	static Foo c;

	@BeforeAll
	static void beforeAll() {
		a = new Foo();
		a.n = 3;

		b = new Foo();
		b.n = 8;

		c = new Foo();
		c.n = 3;
	}

	@Test
	void testDifferent() {
		assertTrue(a.lt(b));
		assertTrue(a.lte(b));
		assertFalse(a.gt(b));
		assertFalse(a.gte(b));
	}

	@Test
	void testSonarCoverage() {
		assertFalse(b.lte(c));
	}

	@Test
	void testEqual() {
		assertFalse(a.lt(c));
		assertTrue(a.lte(c));
		assertFalse(a.gt(c));
		assertTrue(a.gte(c));
	}

	@Test
	void testCompareTo() {
		assertTrue(a.compareTo(b) < 0);
		assertTrue(b.compareTo(a) > 0);
		assertEquals(0, a.compareTo(c));
	}
}
