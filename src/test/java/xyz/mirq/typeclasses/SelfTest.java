package xyz.mirq.typeclasses;

import org.junit.jupiter.api.Test;

class SelfTest {

	public static class Foo implements Self<Foo> {
		@Override
		public Foo self() {
			return this;
		}
	}

	@Test
	@SuppressWarnings("squid:S2699")  // No assertions needed
	void ensureCastNotNeeded() {
		final Self<? extends Foo> self = new Foo();
//		final Foo error = self;
		final Foo foo = self.self();
	}
}
