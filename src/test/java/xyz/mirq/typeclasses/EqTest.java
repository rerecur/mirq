package xyz.mirq.typeclasses;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class EqTest {

	public static class Foo
		implements Eq<Foo> {

		public int n;

		@Override
		public boolean eqv(Foo that) {
			return this == that || this.n == that.n;
		}

		@Override
		public Foo self() {
			return this;
		}
	}

	@Test
	void testEquality() {
		final Foo a = new Foo();
		a.n = 1;

		final Foo b = new Foo();
		b.n = 1;

		assertTrue(a.eqv(b));
		assertFalse(a.neq(b));
	}

	@Test
	void testInequality() {
		final Foo a = new Foo();
		a.n = 1;

		final Foo b = new Foo();
		b.n = 2;

		assertFalse(a.eqv(b));
		assertTrue(a.neq(b));
	}
}
