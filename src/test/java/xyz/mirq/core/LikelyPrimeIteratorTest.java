package xyz.mirq.core;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class LikelyPrimeIteratorTest {

	@Test
	void primesShouldBeOddAfterTwo() {
		final long firstOddNonPrime = 9L;
		final LikelyPrimeIterator primes = new LikelyPrimeIterator();

		long p = primes.nextLong();
		assertEquals(2L, p);

		while (p <= firstOddNonPrime) {
			p = primes.nextLong();
			assertNotEquals(0L, p % 2L);
		}
	}

	@Test
	void testLotsOfPrimes() {
		/* We can't actually see the primes array length because it's private */
		final int presumedBiggerThanPrimesArrayLength = 100;
		final LikelyPrimeIterator primes = new LikelyPrimeIterator();

		long p = primes.nextLong();
		assertEquals(2L, p);

		for (int i = 0; i < presumedBiggerThanPrimesArrayLength; ++i) {
			p = primes.nextLong();
			assertNotEquals(0L, p % 2L);
		}
	}

	@Test
	void hasNextShouldReturnTrue() {
		final LikelyPrimeIterator primes = new LikelyPrimeIterator();
		assertTrue(primes.hasNext());
	}

}
