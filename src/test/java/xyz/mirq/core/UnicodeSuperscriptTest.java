package xyz.mirq.core;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static xyz.mirq.core.Unicode.superscript;

class UnicodeSuperscriptTest {

	@Test
	void testZero() {
		assertEquals("⁰", superscript(0));
	}

	@Test
	void testOne() {
		assertEquals("¹", superscript(1));
	}

	@Test
	void testNegativeOne() {
		assertEquals("⁻¹", superscript(-1));
	}

	@Test
	void testFive() {
		assertEquals("⁵", superscript(5));
	}

	@Test
	void testTwelve() {
		assertEquals("¹⁶²", superscript(162));
	}

	@Test
	void testNegativeThree() {
		assertEquals("⁻³", superscript(-3));
	}

	@Test
	void test4789() {
		assertEquals("⁴⁷⁸⁹", superscript(4789));
	}
}
