package xyz.mirq.core;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static xyz.mirq.core.Algorithms.squareComponent;

class AlgorithmsSquareComponentTest {

	@Test
	void testZero() {
		Assertions.assertThrows(ArithmeticException.class, () -> {
			squareComponent(0);
		});
	}

	@Test
	void testNegative() {
		Assertions.assertThrows(ArithmeticException.class, () -> {
			squareComponent(-4);
		});
	}

	@Test
	void testPrime() {
		final long[] m_k = squareComponent(17);
		assertEquals(1L, m_k[0]);
		assertEquals(17L, m_k[1]);
	}

	@Test
	void testSquare() {
		final long[] m_k = squareComponent(25);
		assertEquals(5L, m_k[0]);
		assertEquals(1L, m_k[1]);
	}

	@Test
	void testComposite() {
		final long[] m_k = squareComponent(360);
		assertEquals(6L, m_k[0]);
		assertEquals(10L, m_k[1]);
	}
}
