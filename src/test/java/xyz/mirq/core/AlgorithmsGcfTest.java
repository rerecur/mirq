package xyz.mirq.core;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static xyz.mirq.core.Algorithms.gcf;

class AlgorithmsGcfTest {

	@Test
	void testBothZeros() {
		assertEquals(0, gcf(0, 0));
	}

	@Test
	void testFirstZero() {
		assertEquals(15, gcf(0, 15));
	}

	@Test
	void testSecondZero() {
		assertEquals(15, gcf(15, 0));
	}

	@Test
	void testBothPositive() {
		assertEquals(6, gcf(12, 18));
	}

	@Test
	void testBothNegative() {
		assertEquals(6, gcf(-12, -18));
	}

	@Test
	void testNegativeAndPositive() {
		assertEquals(6, gcf(-12, 18));
	}

	@Test
	void testPositiveAndNegative() {
		assertEquals(6, gcf(12, -18));
	}

}
