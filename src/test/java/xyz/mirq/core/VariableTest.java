package xyz.mirq.core;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class VariableTest {

	static Variable x;
	static Variable y;
	static Variable anotherX;

	@BeforeAll
	static void beforeAll() {
		x = Variable.of("x");
		y = Variable.of("y");
		anotherX = Variable.of("x");
	}

	@Test
	void testToString() {
		assertEquals("x", x.toString());
	}

	@Test
	void testSelf() {
		assertTrue(x.self() instanceof Variable);
	}

	@Test
	void testHashCode() {
		assertNotEquals(x.hashCode(), y.hashCode());
	}

	@Test
	@SuppressWarnings("squid:S5785") // Deliberately testing equals
	void testEquals() {
		assertTrue(x.equals(x));
		assertFalse(x.equals("x"));
		assertTrue(x.equals(anotherX));
		assertFalse(x.equals(y));
	}

	@Test
	void testEqv() {
		assertTrue(x.eqv(anotherX));
		assertFalse(x.eqv(y));
	}

	@Test
	void testLt() {
		assertTrue(x.lt(y));
		assertFalse(x.lt(anotherX));
	}

}
